using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Utils
{
    public class FilterQuery
    {
        public string keyword { get; set; }
        public string sortDir { get; set; }
        public string sortBy { get; set; }
        public int pageIndex { get; set; }
        public int pageSize { get; set; }
        public FilterQuery()
        {
            this.keyword = string.Empty;
            this.sortDir = string.Empty;
            this.sortBy = string.Empty;
            this.pageIndex = 0;
            this.pageSize = 20;
        }
    }


    public class FilterDepartment : FilterQuery
    {
        public int locationId { get; set; }
        public string languageCode { get; set; }
        public FilterDepartment()
        {
            this.locationId = 0;
            this.languageCode = string.Empty;
        }
    }
    public class FilterProduct : FilterQuery
    {
        public DateTime from { get; set; }
        public DateTime to { get; set; }
        public MI.Entity.Enums.StatusProduct trangThai { get; set; }
        public string languageCode { get; set; }
        public int idZone { get; set; }
        public FilterProduct()
        {
            this.from = DateTime.MinValue;
            this.to = DateTime.MaxValue;
            this.trangThai = MI.Entity.Enums.StatusProduct.All;
            this.languageCode = "vi-VN";
        }
    }
    public class FilterZone : FilterQuery
    {
        public string languageCode { get; set; }
        public MI.Entity.Enums.TypeZone type { get; set; }
        public FilterZone()
        {
            this.languageCode = string.Empty;
            this.type = 0;
        }
    }
    public class FilterArticle : FilterQuery
    {
        public int? Type { get; set; }

        public int? Status { get; set; }
        public string ZoneIds { get; set; }
        public FilterArticle()
        {
            this.Type = 0;
            this.Status = 0;
            this.ZoneIds = "";
        }
    }
}
