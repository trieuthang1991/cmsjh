using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using LazZiya.ImageResize;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MI.Bo.Bussiness;
using MI.Entity.Common;
using MI.Entity.Models;
using Newtonsoft.Json;
using Utils;
using Image = System.Drawing.Image;

namespace JanHome.CMS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FileUploadV2Controller : ControllerBase
    {
        FileUploadBCL _fileUploadBCL;
        private readonly IHostingEnvironment _env;
        private readonly IConfiguration _config;
        public FileUploadV2Controller(IHostingEnvironment env, IConfiguration iConfig)
        {
            _env = env;
            _config = iConfig;
            _fileUploadBCL = new FileUploadBCL();
        }

        [Route("UploadImage")]
        [HttpPost, DisableRequestSizeLimit]
        public IActionResult Upload(List<IFormFile> files)
        {

            string serverPath = _config.GetValue<string>("AppSettings:UploadFolder");
            string folder = string.Format("{0:yyyy/MM/dd}", DateTime.Now);
            var res = new ResponseData();
            try
            {

                var webRoot = _env.WebRootPath;
                var fileDirectory = Path.Combine(serverPath, folder);
                var pathToSave = Path.Combine(webRoot, fileDirectory);

                if (Request.Form.Files.Count > 0)
                {
                    var uploadResults = new List<FileInfo>();
                    string imageAllowUpload = _config.GetValue<string>("AppSettings:ImageAllowUpload");
                    string documentAllowUpload = _config.GetValue<string>("AppSettings:DocumentAllowUpload");
                    bool fileUploadSubFix = _config.GetValue<bool>("AppSettings:FileUploadSubFix");
                    int fileUploadMaxSize = _config.GetValue<int>("AppSettings:FileUploadMaxSize");
                    int imageScaleWidth = _config.GetValue<int>("AppSettings:ImageScaleByWidth");
                    int imageScaleHeight = _config.GetValue<int>("AppSettings:ImageScaleHeight");

                    foreach (var file in Request.Form.Files)
                    {
                        var ext = Path.GetExtension(fileDirectory + "\\" + file.FileName);

                        var imageAllowFileArray = imageAllowUpload.Split(',');
                        var documentAllowFileArray = documentAllowUpload.Split(',');
                        if (Array.IndexOf(imageAllowFileArray, ext.ToLower()) != -1 || Array.IndexOf(documentAllowFileArray, ext.ToLower()) != -1)
                        {
                            var fileName = Path.GetFileName(fileDirectory + "\\" + file.FileName);
                            if (fileUploadMaxSize >= (file.Length / 1024))
                            {

                                if (fileUploadSubFix)
                                {
                                    string filenameNoExtension = System.IO.Path.GetFileNameWithoutExtension(fileName);
                                    fileName = Utility.UnicodeToKoDauAndGach(filenameNoExtension) + "-" + DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Second + DateTime.Now.Millisecond + ext;
                                }
                                var fullPath = Path.Combine(pathToSave, fileName);
                                if (!Directory.Exists(pathToSave))
                                {
                                    Directory.CreateDirectory(pathToSave);
                                }

                                using (var stream = new FileStream(fullPath, FileMode.Create))
                                {
                                    file.CopyTo(stream);
                                    // var image = Image.FromFile(Server.MapPath("~/" + fileStoragePath + "/" + generatedFilename));
                                    // var dimensions = image.PhysicalDimension.Width + "x" + image.PhysicalDimension.Height;


                                    var obj = new FileUpload();
                                    obj.Name = fileName;
                                    obj.FilePath = "/" + Path.Combine(folder).Replace("\\", "/") + "/" + fileName;
                                    obj.FileExt = ext;
                                    //   obj.Dimensions = dimensions;
                                    obj.FileSize = Math.Round((double)file.Length / 1024000, 1);
                                    obj.Status = 1;
                                    obj.Type = 2;


                                    ResponseData responseData = new ResponseData();
                                    try
                                    {
                                        bool result = _fileUploadBCL.Add(obj);
                                        if (result)
                                        {

                                            responseData.Success = true;
                                            responseData.Message = "Thành công";
                                            FileInfo fileInfo = new FileInfo
                                            {
                                                name = fileName,
                                                path = "/" + Path.Combine(folder).Replace("\\", "/") + "/" + fileName,
                                                ext = ext,
                                                size = Math.Round((double)file.Length / 1024000, 1),
                                                code = 200,
                                                messages = "ok"

                                            };
                                            uploadResults.Add(fileInfo);
                                            if (Array.IndexOf(imageAllowFileArray, ext.ToLower()) != -1)
                                            {
                                                using (var _stream = file.OpenReadStream())
                                                {
                                                    var uploadedImage = Image.FromStream(_stream);
                                                    var thumbPathToSave = Path.Combine(webRoot, serverPath, "thumb", folder);
                                                    if (!Directory.Exists(thumbPathToSave))
                                                    {
                                                        Directory.CreateDirectory(thumbPathToSave);
                                                    }

                                                    var thumbFileDirectory = Path.Combine(folder);
                                                    thumbFileDirectory = thumbFileDirectory.Replace("/", "\\");
                                                    var thumbPath =
                                                         "wwwroot\\" + serverPath + "\\thumb\\" + thumbFileDirectory +
                                                        "\\" + fileName;

                                                    if (imageScaleWidth > 0 && imageScaleHeight > 0)
                                                    {
                                                        var img = ImageResize.Scale(uploadedImage, 200, 100);
                                                        img.SaveAs(@"" + thumbPath + "");
                                                    }

                                                    if (imageScaleWidth > 0 && imageScaleHeight == 0)
                                                    {
                                                        var img = ImageResize.ScaleByWidth(uploadedImage, imageScaleWidth);
                                                        img.SaveAs(@"" + thumbPath + "");
                                                    }

                                                }


                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {

                                    }

                                    //   return responseData;
                                    //  context.Response.Write(JsonConvert.SerializeObject(fileInfo));
                                }
                            }
                            else
                            {
                                FileInfo fileInfo = new FileInfo
                                {
                                    name = fileName,
                                    path = "/",
                                    ext = ext,
                                    size = Math.Round((double)file.Length / 1024000, 1),
                                    code = 900,
                                    messages = "Limited file size"

                                };
                                uploadResults.Add(fileInfo);
                            }

                        }
                        else
                        {
                            FileInfo fileInfo = new FileInfo
                            {
                                name = file.FileName,
                                path = "/",
                                ext = ext,
                                size = Math.Round((double)file.Length / 1024000, 1),
                                code = 900,
                                messages = "File extension not allowed"

                            };
                            uploadResults.Add(fileInfo);
                        }



                        res.Success = true;

                    }
                }
                else
                {
                    res.ErrorCode = -1;
                    res.Success = false;
                    res.Message = "Hãy chọn một file.";
                }
                return Ok(res);
            }
            catch (Exception ex)
            {

                res.ErrorCode = -2;
                res.Success = false;
                res.Message = "Lỗi hệ thống, Vui lòng liên hệ quản trị.";
                return Ok(res);
            }

        }

    }

    public class FileInfo
    {
        public int code { get; set; }
        public string name { get; set; }
        public string path { get; set; }
        public double size { get; set; }
        public string ext { get; set; }
        public string messages { get; set; }
    }
}
