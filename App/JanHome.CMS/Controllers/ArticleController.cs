using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MI.Bo.Bussiness;
using MI.Dapper.Data.Models;
using MI.Dapper.Data.Repositories.Interfaces;
using MI.Entity.Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Utils;

namespace JanHome.CMS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArticleController : ControllerBase
    {
        ArticleBCL _articleBcl;
        private readonly IArticlesRepository _articlesRepository;
        private readonly IZoneArticleRepository _zoneArticleRepository;
        private readonly IProductRepository _productRepository;

        public ArticleController(IArticlesRepository articlesRepository,
            IZoneArticleRepository zoneArticleRepository, IProductRepository productRepository
        )
        {
            _articlesRepository = articlesRepository;
            _zoneArticleRepository = zoneArticleRepository;
            _productRepository = productRepository;
            _articleBcl = new ArticleBCL();
        }

        [HttpGet("GetAll")]
        public ResponseData GetAll()
        {
            var responseData = new ResponseData();
            try
            {
                var data = _articleBcl.FindAll().ToList();
                responseData.Success = true;
                responseData.Message = "Thành công";
                responseData.ListData = data.ToList<object>();
            }
            catch (Exception ex)
            {
            }

            return responseData;
        }


        [HttpGet("GetPage")]
        public async Task<IActionResult> GetPage([FromQuery] FilterArticle filter)
        {
            try
            {
                var data = await _articlesRepository.GetAllPaging(filter);
                return Ok(data);
            }
            catch (Exception ex)
            {
            }

            return Ok();
        }


        [HttpGet("GetById")]
        public async Task<ResponseData> GetById(int id)
        {
            var responseData = new ResponseData();
            try
            {
                var result = await _articlesRepository.GetById(id);
                if (result != null)
                {
                    responseData.Success = true;
                    responseData.Message = "Thành công";
                    responseData.Data = result;
                }
            }
            catch (Exception ex)
            {
            }

            return responseData;
        }

        [HttpPost("Add")]
        public async Task<ResponseData> Add([FromBody] Articles article)
        {
            var responseData = new ResponseData();
            try
            {
                var result = await _articlesRepository.Create(article);
                if (result > 0)
                {
                    responseData.Success = true;
                    responseData.Message = "Thành công";
                }
            }
            catch (Exception ex)
            {
            }

            return responseData;
        }

        [HttpPut("Update")]
        public async Task<ResponseData> Update([FromBody] Articles ads)
        {
            var responseData = new ResponseData();
            try
            {
                var result = await _articlesRepository.Update(ads);
                if (result > 0)
                {
                    responseData.Success = true;
                    responseData.Message = "Thành công";
                }
            }
            catch (Exception ex)
            {
            }

            return responseData;
        }

        [HttpDelete("Delete")]
        public ResponseData Delete(int id)
        {
            var responseData = new ResponseData();
            try
            {
                var ads = _articleBcl.Remove(id);
                if (ads)
                {
                    responseData.Success = true;
                    responseData.Message = "Thành công";
                }
            }
            catch (Exception ex)
            {
            }

            return responseData;
        }

        [HttpGet("GetZoneArticle")]
        public async Task<List<Group>> GetZoneArticle()
        {
            try
            {
                var result = await _zoneArticleRepository.GetAllZoneArticles();
                if (result != null)
                {
                    return result;
                }
            }
            catch (Exception ex)
            {
            }

            return new List<Group>();
        }

        [HttpGet("GetProductAtArticle")]
        public async Task<List<ProductAtArticle>> GetProductAtArticle()
        {
            try
            {
                var result = await _productRepository.GetAllProduct();
                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
