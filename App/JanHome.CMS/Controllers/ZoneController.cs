using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MI.Entity.Common;
using MI.Bo.Bussiness;
using MI.Entity.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utils;

namespace JanHome.CMS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ZoneController : ControllerBase
    {
        ZoneBCL zoneBCL;
        ZoneInLanguageBCL zoneLangBCL;
        public ZoneController()
        {
            zoneBCL = new ZoneBCL();
            zoneLangBCL = new ZoneInLanguageBCL();
        }

        [HttpGet("GetAll")]
        public ResponseData GetAll(string tuKhoa, string languageCode = "vi-VN", MI.Entity.Enums.StatusZone trangThai = MI.Entity.Enums.StatusZone.All, MI.Entity.Enums.TypeZone loai = MI.Entity.Enums.TypeZone.All)
        {
            ResponseData responseData = new ResponseData();
            try
            {
                var data = zoneLangBCL.FindAll(tuKhoa, trangThai, loai, languageCode);
                responseData.Success = true;
                responseData.Message = "Thành công";
                responseData.ListData = data.Select(x => new { Id = x.ZoneId, Name = x.Name, SortOrder = x.Zone.SortOrder ?? 1, ParentId = x.Zone.ParentId }).ToList<object>();
            }
            catch (Exception ex)
            {

            }
            return responseData;
        }

        [HttpGet("Supports")]
        public ResponseSuports Supports()
        {
            ResponseSuports responseData = new ResponseSuports();
            try
            {
                var lstTypes = MI.Entity.EnumHelper.ToList(typeof(MI.Entity.Enums.TypeZone), true);
                var lstStatus = MI.Entity.EnumHelper.ToList(typeof(MI.Entity.Enums.StatusZone), true);
                responseData.ListTypes = lstTypes.Cast<object>().ToList();
                responseData.ListStatus = lstStatus.Cast<object>().ToList();
                //responseData.Success = true;
                //responseData.Message = "Thành công";
                //responseData.ListData =
            }
            catch (Exception ex)
            {

            }
            return responseData;
        }
        [HttpGet("Get")]
        public ResponsePaging Get([FromQuery]FilterZone filter)
        {
            ResponsePaging responseData = new ResponsePaging();
            try
            {
                int total = 0;
                var data = zoneBCL.Get(filter, out total);
                responseData.ListData = data.ToList<object>();
                responseData.Total = total;
            }
            catch (Exception ex)
            {

            }
            return responseData;
        }


        [HttpGet("GetById")]
        public ResponseData GetById(int id)
        {
            ResponseData responseData = new ResponseData();
            try
            {
                var ads = zoneBCL.FindById(x => x.Id == id, x => x.ZoneInLanguage);
                if (ads != null)
                {
                    responseData.Success = true;
                    responseData.Message = "Thành công";
                    responseData.ListData = ads.ZoneInLanguage.ToList<object>();
                    ads.ZoneInLanguage = new List<ZoneInLanguage>();
                    responseData.Data = ads;
                }
            }
            catch (Exception ex)
            {

            }
            return responseData;
        }
        [HttpPost("Add")]
        public ResponseData Add([FromBody]Zone ads)
        {
            ResponseData responseData = new ResponseData();
            try
            {
                var result = zoneBCL.AddReturnObj(ads);
                if (result != null && result.Id > 0)
                {
                    responseData.Id = result.Id;
                    responseData.Success = true;
                    responseData.Message = "Thành công";
                }
            }
            catch (Exception ex)
            {

            }

            return responseData;
        }


        [HttpPut("Update")]
        public ResponseData Update([FromBody]Zone ads)
        {
            ResponseData responseData = new ResponseData();
            try
            {
                bool result = zoneBCL.Update(ads);
                if (result)
                {
                    responseData.Success = true;
                    responseData.Message = "Thành công";
                }
            }
            catch (Exception ex)
            {

            }

            return responseData;
        }

        [HttpPost("MergeLang")]
        public ResponseData Merge([FromBody]ZoneInLanguage ads)
        {
            ResponseData responseData = new ResponseData();
            try
            {
                bool result = zoneLangBCL.Merge(ads);
                if (result)
                {
                    responseData.Success = true;
                    responseData.Message = "Thành công";
                }
            }
            catch (Exception ex)
            {

            }

            return responseData;
        }

        [HttpDelete("Delete")]
        public ResponseData Delete(int id)
        {
            ResponseData responseData = new ResponseData();
            try
            {
                bool ads = zoneBCL.UpdateTrangThai(new Zone { Id = id, Status = (byte)MI.Entity.Enums.StatusZone.Delete });
                if (ads)
                {
                    responseData.Success = true;
                    responseData.Message = "Thành công";
                }
            }
            catch (Exception ex)
            {

            }

            return responseData;
        }
    }
}
