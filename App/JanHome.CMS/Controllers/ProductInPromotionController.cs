using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MI.Bo.Bussiness;
using MI.Entity.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace JanHome.CMS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductInPromotionController : ControllerBase
    {
        ProductInPromotionBCL productInPromotionBCL;
        PromotionInLanguageBCL promotionInLanguageBCL;
        public ProductInPromotionController()
        {
            productInPromotionBCL = new ProductInPromotionBCL();
            promotionInLanguageBCL = new PromotionInLanguageBCL();
        }


        [HttpGet("GetByProductId")]
        public ResponseTwoList GetByProductId(int idProduct)
        {
            ResponseTwoList responseData = new ResponseTwoList();
            try
            {
                var promotion = promotionInLanguageBCL.FindAll(x => x.LanguageCode.Trim() == "vi-VN").ToDictionary(x => x.PromotionId, x => x);
                var promotioninProduct = productInPromotionBCL.FindAll(x => x.ProductId == idProduct);

                responseData.ListObj1 = promotion.Values.Where(x =>
                !promotioninProduct.Select(d => d.PromotionId).Contains(x.PromotionId)).Select(x => new { Id = 0, PromotionId = x.PromotionId, ProductId = idProduct, Name = x.Name, Des = x.Description }).ToList<object>();
                responseData.ListObj2 = promotioninProduct.Select(x => new { x.Id, PromotionId = x.PromotionId, ProductId = x.ProductId, Name = promotion.ContainsKey(x.PromotionId) ? promotion[x.PromotionId].Name : "", Des = promotion.ContainsKey(x.PromotionId) ? promotion[x.PromotionId].Description : "" }).ToList<object>();

            }
            catch (Exception ex)
            {

            }
            return responseData;
        }



    }
}
