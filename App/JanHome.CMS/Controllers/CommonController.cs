using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MI.Dapper.Data.Constant;
using MI.Dapper.Data.Models;
using MI.Dapper.Data.Repositories.Interfaces;
using MI.Dapper.Data.ViewModels;
using MI.Entity;
using MI.Entity.Common;
using MI.Entity.Enums;
using Microsoft.AspNetCore.Mvc;

namespace JanHome.CMS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommonController : ControllerBase
    {
        private readonly ILanguageRepository _languageRepository;
        private readonly IManufacturerRepository _manufacturerRepository;

        public CommonController(ILanguageRepository languageRepository, IManufacturerRepository manufacturerRepository)
        {
            _languageRepository = languageRepository;
            _manufacturerRepository = manufacturerRepository;
        }

        [HttpGet("GetAllLanguageOptions")]
        public async Task<List<Languages>> GetAllLanguageOptions()
        {
            try
            {
                var result = await _languageRepository.GetAllLanguage();
                return result;
            }
            catch (Exception e)
            {
            }

            return new List<Languages>();
        }


        [HttpGet("GetAllManufacturerOptions")]
        public async Task<List<Manufacturers>> GetAllManufacturerOptions()
        {
            try
            {
                var result = await _manufacturerRepository.GetAllManufacturer();
                return result;
            }
            catch (Exception e)
            {
            }

            return new List<Manufacturers>();
        }

        [HttpGet("GetAllStatusOptions")]
        public List<StatusViewModel> GetAllStatusOptions()
        {
            try
            {
                var result = ListContant.ListStatusViewModel;
                return result;
            }
            catch (Exception e)
            {
            }

            return new List<StatusViewModel>();
        }

        [HttpGet("GetAllTypeOptions")]
        public List<TypeViewModel> GetAllTypeOptions()
        {
            try
            {
                var result = ListContant.ListTypeViewModel;
                return result;
            }
            catch (Exception e)
            {
            }

            return new List<TypeViewModel>();
        }
        [HttpGet("ProductUnitGet")]
        public ResponseData ProductUnitGet()
        {
            ResponseData responseData = new ResponseData();
            try
            {
                var lstProductUnit = MI.Entity.EnumHelper.ToList(typeof(MI.Entity.Enums.ProductUnit), true);
                responseData.ListData = lstProductUnit.Cast<object>().ToList();
            }
            catch (Exception ex)
            {

            }
            return responseData;
        }
    }
}
