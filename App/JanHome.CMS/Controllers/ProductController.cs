using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MI.Entity.Common;
using MI.Bo.Bussiness;
using MI.Entity.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MI.Dapper.Data.Repositories.Interfaces;
using Utils;
using System.Text;

namespace JanHome.CMS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        ProductInLanguageBCL productInLanguageBCL;
        LocationBCL locationBCL;
        ZoneInLanguageBCL zoneInLanguageBCL;
        ProductPriceInLocationBCL priceInLocationBCL;
        ProductBCL productBCL;
        TagInProductBCL tagInProductBCL;
        public ProductController(IProductRepository productRepository)
        {
            productBCL = new ProductBCL();
            productInLanguageBCL = new ProductInLanguageBCL();
            zoneInLanguageBCL = new ZoneInLanguageBCL();
            tagInProductBCL = new TagInProductBCL();
            priceInLocationBCL = new ProductPriceInLocationBCL();
            locationBCL = new LocationBCL();
        }

        [HttpGet("GetAll")]
        public ResponseData GetAll()
        {
            ResponseData responseData = new ResponseData();
            try
            {


                var data = productBCL.FindAll().ToList();
                responseData.Success = true;
                responseData.Message = "Thành công";
                responseData.ListData = data.ToList<object>();

            }
            catch (Exception ex)
            {

            }
            return responseData;
        }
        [HttpGet("Get")]
        public ResponsePaging Get(int pageIndex, int pageSize, string sortBy, string sortDir)
        {
            ResponsePaging responseData = new ResponsePaging();
            try
            {
                int total = 0;
                var data = productBCL.Get(pageIndex, pageSize, sortBy, sortDir, out total);
                responseData.ListData = data.ToList<object>();
                responseData.Total = total;
            }
            catch (Exception ex)
            {

            }
            return responseData;
        }

        [HttpGet("GetPriceByLocation")]
        public ResponseTwoList GetPriceByLocation(int idProduct)
        {
            ResponseTwoList responseData = new ResponseTwoList();
            try
            {
                var locations = locationBCL.FindAll().ToDictionary(x => x.Id, x => x);
                var locationPrice = priceInLocationBCL.FindAll(x => x.ProductId == idProduct);

                responseData.ListObj1 = locations.Values.Where(x =>
                !locationPrice.Select(d => d.LocationId).Contains(x.Id)).Select(x => new { Id = 0, LocationId = x.Id, Price = 0, SalePrice = 0, Ten = x.Name, ProductId = idProduct }).ToList<object>();
                responseData.ListObj2 = locationPrice.Select(x => new { x.Id, x.LocationId, x.Price, x.ProductId, x.SalePrice, Ten = locations.ContainsKey(x.LocationId) ? locations[x.LocationId].Name : "" }).ToList<object>();

                //int total = 0;
                //var data = productBCL.Get(pageIndex, pageSize, sortBy, sortDir, out total);
                //responseData.ListData = data.ToList<object>();
                //responseData.Total = total;
            }
            catch (Exception ex)
            {

            }
            return responseData;
        }


        [HttpGet("Search")]
        public ResponsePaging Search([FromQuery]FilterProduct filter)
        {
            ResponsePaging responseData = new ResponsePaging();
            try
            {
                int total = 0;

                var data = productInLanguageBCL.FindAll(filter, out total);
                var lstId = data.Select(x => x.Product.ProductInZone).SelectMany(d => d).ToList().Select(x => x.ZoneId);
                var dicZone = zoneInLanguageBCL.GetById(lstId.Distinct().ToList(), filter.languageCode).ToDictionary(x => x.ZoneId, z => z.Name);

                responseData.ListData = data.Select(x => new { id = x.ProductId, name = x.Title, avatar = Utils.Settings.AppSettings.FoderImg + x.Product.Avatar, price = x.Product.Price, category = BuildZone(x.Product.ProductInZone, dicZone) }).ToList<object>();
                responseData.Total = total;
            }
            catch (Exception ex)
            {

            }
            return responseData;
        }





        public string BuildZone(List<ProductInZone> lstObj, Dictionary<int, string> dic)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var item in lstObj)
            {
                if (dic.ContainsKey(item.ZoneId))
                {
                    sb.Append(dic[item.ZoneId] + "<br/>");
                }
            }
            return sb.ToString(); ;
        }

        [HttpGet("GetById")]
        public ResponseData GetById(int id)
        {
            ResponseData responseData = new ResponseData();
            try
            {
                var ads = productBCL.FindById(id);
                ads.Avatar = "http://localhost:60613/uploads/images/" + ads.Avatar;
                ads.ProductInLanguage = productInLanguageBCL.GetByProductId(id, "vi-VN");
                if (ads != null)
                {
                    responseData.Success = true;
                    responseData.Message = "Thành công";
                    responseData.Data = ads;
                }
            }
            catch (Exception ex)
            {

            }
            return responseData;
        }
        [HttpPost("Add")]
        public ResponseData Add([FromBody]Product ads)
        {
            ResponseData responseData = new ResponseData();
            try
            {
                int result1 = productBCL.CreateProduct(ads);
                if (result1 > 0)
                {
                    responseData.Data = new { productID = result1 };
                    responseData.Success = true;
                    responseData.Message = "Thành công";


                }
                else
                {
                    responseData.Success = false;
                    responseData.Message = "Thất bại";
                }
            }
            catch (Exception ex)
            {

            }

            return responseData;
        }
        [HttpPut("Update")]
        public ResponseData Update([FromBody]Product ads)
        {
            ResponseData responseData = new ResponseData();
            try
            {
                bool result = productBCL.Update(ads);
                if (result)
                {
                    responseData.Success = true;
                    responseData.Message = "Thành công";
                }
            }
            catch (Exception ex)
            {

            }

            return responseData;
        }

        [HttpDelete("Delete")]
        public ResponseData Delete(int id)
        {
            ResponseData responseData = new ResponseData();
            try
            {
                bool ads = productBCL.UpdateTrangThai(new Product() { Id = id, Status = (byte)MI.Entity.Enums.StatusZone.Delete });
                if (ads)
                {
                    responseData.Success = true;
                    responseData.Message = "Thành công";
                }
                else
                {
                    responseData.Success = false;
                    responseData.Message = "Thất bại";
                }
            }
            catch (Exception ex)
            {

            }

            return responseData;
        }
    }
}
