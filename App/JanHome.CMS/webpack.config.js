const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const bundleOutputDir = './wwwroot/dist';
const merge = require('webpack-merge');
const prodn = process.env.NODE_ENV === 'production'
//process.traceDeprecation = true;
process.noDeprecation = true;

module.exports = (env) => {
    const isDevBuild = !(env && env.prod);
    const sharedConfig = () => ({

        performance: {
            hints: false
        },
        stats: { modules: false },
        // entry: { 'main': './ClientApp/entry-client.js' },
        resolve: {
            extensions: ['.js', '.vue'],
            //alias: {
            //    'vue$': 'vue/dist/vue',
            //    'components': path.resolve(__dirname, './ClientApp/components'),
            //    'views': path.resolve(__dirname, './ClientApp/views'),
            //    'utils': path.resolve(__dirname, './ClientApp/utils'),
            //    'api': path.resolve(__dirname, './ClientApp/store/api')
            //}
        },
        output: {
            path: path.join(__dirname, bundleOutputDir),
            filename: '[name].js',
            publicPath: '/dist/'
        },
        module: {
            rules: [
                //{ test: /\.vue$/, include: /ClientApp/, use: 'vue-loader' },
                { test: /\.js$/, include: /ClientApp/, use: 'babel-loader' },
                { test: /\.css$/, use: isDevBuild ? ['style-loader', 'css-loader'] : ExtractTextPlugin.extract({ use: 'css-loader' }) },
                {
                    test: /\.vue$/,
                    include: /ClientApp/,
                    loader: 'vue-loader',
                    options: {
                        loaders: {
                            // Since sass-loader (weirdly) has SCSS as its default parse mode, we map 
                            // the "scss" and "sass" values for the lang attribute to the right configs here. 
                            // other preprocessors should work out of the box, no loader config like this necessary. 
                            'scss': [
                                'vue-style-loader',
                                'css-loader',
                                {
                                    loader: "sass-loader",
                                    //options: {
                                    //    includePaths: [
                                    //        path.resolve(__dirname, "./node_modules")
                                    //    ]
                                    //}
                                }
                            ]

                        }
                    }
                },
                { test: /\.(png|jpg|jpeg|gif|svg)$/, use: 'url-loader?limit=25000' },
                {
                    test: /\.(woff|woff2|eot|ttf|svg)(\?.*$|$)/,
                    loader: 'file-loader'
                }
            ],

        },

        plugins: [
            //new webpack.DllReferencePlugin({
            //    context: __dirname,
            //    manifest: require('./wwwroot/dist/vendor-manifest.json')
            //})
        ].concat(isDevBuild ? [
            // Plugins that apply in development builds only
            new webpack.SourceMapDevToolPlugin({
                filename: '[file].map', // Remove this line if you prefer inline source maps
                moduleFilenameTemplate: path.relative(bundleOutputDir, '[resourcePath]') // Point sourcemap entries to the original file locations on disk
            })
        ] : [
                // Plugins that apply in production builds only
                new webpack.optimize.UglifyJsPlugin(),
                new ExtractTextPlugin('site.css')
            ])
    });


    const clientBundleConfig = merge(sharedConfig(), {
        entry: { 'main-client': './ClientApp/entry-client.js' },
        output: {
            path: path.join(__dirname, bundleOutputDir)
        }
    });

    const serverBundleConfig = merge(sharedConfig(), {
        target: 'node',
        entry: { 'main-server': './ClientApp/entry-server.js' },
        output: {
            libraryTarget: 'commonjs2',
            path: path.join(__dirname, bundleOutputDir)
        },
        module: {
            rules: [
                {
                    test: /\.json?$/,
                    loader: 'json-loader',
                    exclude: [/lang/, /wwwroot/, /node_modules/]
                }
            ]
        },
    });

    return [clientBundleConfig, serverBundleConfig];
    // return [clientBundleConfig];
};
