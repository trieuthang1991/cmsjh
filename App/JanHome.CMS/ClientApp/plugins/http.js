//import Vue from 'vue';
import axios from 'axios';
import {authenticationRepository} from "../repository/authentication/authenticationRepository";
//import { getCookie } from '../utils';
//https://alligator.io/vuejs/rest-api-axios/
let config = require('./../../appsettings.json');

const HttpService = axios.create({
    baseURL: config.AppSettings.Domain,
    headers: {...headers()}
});

function headers() {
    const currentUser = authenticationRepository.currentUserValue || {};
    const authHeader = currentUser.token ? {'Authorization': 'Bearer ' + currentUser.token} : {};
    return {
        headers: {
            ...authHeader,
            'Content-Type': 'application/json'
        }
    }
}

export default HttpService;
