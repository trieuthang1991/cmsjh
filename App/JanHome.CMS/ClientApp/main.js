import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import store from './store'
import {sync} from 'vuex-router-sync'
import App from './App.vue'
import BootstrapVue from 'bootstrap-vue'
import CoreuiVue from '@coreui/vue';
import debounce from 'lodash/debounce';
import cloneDeep from 'lodash/cloneDeep';
import Toast from "vue-toastification";
import Loading from 'vue-loading-overlay';
import vSelect from 'vue-select'
import CKEditor from '@ckeditor/ckeditor5-vue';
import Multiselect from 'vue-multiselect';
import vMultiselectListbox from 'vue-multiselect-listbox'

import Alertifyjs from 'vue2-alertifyjs'

import Vuelidate from 'vuelidate';

Vue.use(Loading,
    {})

// Import the CSS or use your own!
//https://github.com/Maronato/vue-toastification

import "vue-toastification/dist/index.css";
import "vue-multiselect/dist/vue-multiselect.min.css";
import {router} from "./router";

import 'alertifyjs/build/alertify.min.js'
import 'alertifyjs/build/css/alertify.min.css'
import 'alertifyjs/build/css/themes/default.min.css'

Vue.component('v-select', vSelect);

Vue.component('multiselect', Multiselect);

Vue.component('v-multiselect-listbox', vMultiselectListbox);

const options = {
    position: "top-right",
    timeout: 2769,
    closeOnClick: true,
    pauseOnFocusLoss: true,
    pauseOnHover: true,
    draggable: true,
    draggablePercent: 0.1,
    hideCloseButton: false,
    hideProgressBar: false,
    icon: true,
    transition: "Vue-Toastification__fade",
    maxToasts: 8,
    newestOnTop: true
};

const opts = {
    // dialogs defaults
    autoReset: true,
    basic: false,
    closable: true,
    closableByDimmer: true,
    frameless: false,
    maintainFocus: true, // <== global default not per instance, applies to all dialogs
    maximizable: true,
    modal: true,
    movable: true,
    moveBounded: false,
    overflow: true,
    padding: true,
    pinnable: true,
    pinned: true,
    preventBodyShift: false, // <== global default not per instance, applies to all dialogs
    resizable: true,
    startMaximized: false,
    transition: 'pulse',

    // notifier defaults
    notifier: {
        // auto-dismiss wait time (in seconds)  
        delay: 5,
        // default position
        position: 'bottom-right',
        // adds a close button to notifier messages
        closeButton: false
    },

    // language resources 
    glossary: {
        // dialogs default title
        title: 'AlertifyJS',
        // ok button text
        ok: 'OK',
        // cancel button text
        cancel: 'Cancel'
    },

    // theme settings
    theme: {
        // class name attached to prompt dialog input textbox.
        input: 'ajs-input',
        // class name attached to ok button
        ok: 'ajs-ok',
        // class name attached to cancel button 
        cancel: 'ajs-cancel'
    }
};

Vue.use(CoreuiVue);
Vue.use(Toast, options);
Vue.prototype.$http = axios;
sync(store, router);
Vue.use(BootstrapVue);
Vue.use(Vuex);
Vue.use(debounce);
Vue.use(cloneDeep);
Vue.use(CKEditor);

Vue.use(Alertifyjs, opts);

Vue.use(Vuelidate);


window._ = require('lodash');

const app = new Vue({
    store,
    router,
    ...App
});

export {
    app,
    router,
    store
}
