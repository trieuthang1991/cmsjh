import HttpService from '../../../plugins/http'
import {config} from 'vue-test-utils';

const getArticles = ({commit}, data) => {
    HttpService.get(`/api/Article/Get?pageIndex=${data.pageIndex}&pageSize=${data.pageSize}`, {}).then(response => {
        commit("GET_ARTICLES", {...response.data})
    }).catch(e => {
        alert('ex found:' + e)
    })
};
const getArticle = ({commit}, data) => {
    HttpService.get(`/api/Article/GetById?Id=${data}`, {}).then(response => {
        commit("GET_ARTICLE", {...response.data})
    }).catch(e => {
        alert('ex found:' + e)
    })
};
const getPageArticle = ({commit}, data) => {
    HttpService.get(`/api/Article/GetPage?pageIndex=${data.pageIndex}&pageSize=${data.pageSize}&status=${data.status}&keyword=${data.title}&type=${data.type}&zoneIds=${data.zoneIds}`, {}).then(response => {
        console.log(response.data);
        commit("GET_PAGE_ARTICLES", {...response.data})
    }).catch(e => {
        alert('ex found:' + e)
    })
};

const addArticle = ({commit}, data) => {
    return HttpService.post(`/api/Article/Add`,
        data
    ).then(response => {
        return response.data;
    }).catch(e => {
        alert('ex found:' + e)
    })
};

const updateArticle = ({commit}, data) => {
    return HttpService.put(`/api/Article/Update`,
        data
    ).then(response => {
        return response.data
    }).catch(e => {
        alert('ex found:' + e)
    })
};

const deleteArticle = ({commit}, data) => {
    return HttpService.delete(`/api/Article/Delete?id=${data.Id}`)
        .then(response => {
            return response.data;
        })
        .catch(e => {
            alert('ex found:' + e)
        })
};

export default {
    getArticles,
    getArticle,
    updateArticle,
    deleteArticle,
    getPageArticle,
    addArticle
}
