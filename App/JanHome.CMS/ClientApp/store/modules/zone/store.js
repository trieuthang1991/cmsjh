import actions from './actions'

const _zonestore = {
    state: {
        zones: [{}],
        zone: ''
    },

    mutations: {
        SET_LOADING: (state, payload) => {
            state.isLoading = payload;
        },
        GET_ZONES: (state, payload) => {
            state.zones = payload;
        },
        GET_ZONE: (state, payload) => {
            state.zone = payload.Data;
        },
        UPDATE_ZONE: (state, payload) => {
            state.isOR = payload.data;
        },
        ADD_ZONE: (state, payload) => {
            state.isOR = payload.data;
        },
        DELETE_ZONE: (state, payload) => {
            state.isOR = payload.data;
        },
    },
    actions,
    getters: {
        zones: state => state.zones,
        zone: state => state.zone,
       
    }
}
export default _zonestore;
