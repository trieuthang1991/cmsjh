import HttpService from '../../../plugins/http'
import { config } from 'vue-test-utils';

const updateLocations = ({ commit }, data) => {
    return HttpService.post(`/api/ProductPriceInLocation/Add`, data).then(response => {
        return response.data
    }).catch(e => {
        alert('ex found:' + e)
    })
}
const addCombo = ({ commit }, data) => {
    return HttpService.post(`/api/ComBoProduct/Add`, data).then(response => {
        return response.data
    }).catch(e => {
        alert('ex found:' + e)
    })
}
const getPriceByLocations = ({ commit }, id) => {
    return HttpService.get(`/api/ProductPriceInLocation/GetPriceByLocation?idProduct=${id}`, {
    }).then(response => {
        //console.log('ProductById'+id+response.data);
        return response.data;
    }).catch(e => {
        alert('ex found:' + e)
    })
}

const getProductForCombo = ({ commit }, data) => {
    return HttpService.get(`/api/ComBoProduct/Search?keyword=${data.keyword}&idZone=${data.idZone}&pageSize=${data.pageSize}&pageIndex=${data.pageIndex}`, {
    }).then(response => {
        //console.log('ProductById'+id+response.data);
        return response.data;
    }).catch(e => {
        alert('ex found:' + e)
    })
}

const getPromotionByProducts = ({ commit }, id) => {
    return HttpService.get(`/api/ProductInPromotion/GetByProductId?productId=${id}`, {
    }).then(response => {
        //console.log('ProductById'+id+response.data);
        return response.data;
    }).catch(e => {
        alert('ex found:' + e)
    })
}

const getComboById = ({ commit }, id) => {
    return HttpService.get(`/api/ComBoProduct/GetByProductId?productId=${id}`, {
    }).then(response => {
        //console.log('ProductById'+id+response.data);
        return response.data;
    }).catch(e => {
        alert('ex found:' + e)
    })
}
export default {
    updateLocations,
    getProductForCombo,
    addCombo,
    getComboById,
    getPriceByLocations,
    getPromotionByProducts
}
