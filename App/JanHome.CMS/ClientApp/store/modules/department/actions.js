import HttpService from '../../../plugins/http'
import { config } from 'vue-test-utils';

const getDepartments = ({ commit }, data) => {
    HttpService.get(`/api/Department/Get?pageIndex=${data.pageIndex}&pageSize=${data.pageSize}&keyword=${data.keyword}&locationId=${data.locationId}&languageCode=${data.languageCode}&sortBy=${data.sortBy}&sortDir=${data.sortDir}`, {
    }).then(response => {
        commit("GET_DEPARTMENTS", { ...response.data })
    }).catch(e => {
        alert('ex found:' + e)
    })
}

const getDepartment = ({ commit }, id) => {
    return HttpService.get(`/api/Department/GetById?id=${id}`, {
    }).then(response => {
        return response.data;
        //commit("GET_Department", { ...response.data })
    }).catch(e => {
        alert('ex found:' + e)
    })
}

const addDepartment = ({ commit }, params) => {
    return HttpService.post('/api/Department/add', params)
        .then(response => {
            return response.data;
        })
        .catch(e => {
            alert('ex found:' + e)
        })
}
const editDepartment = ({ commit }, params) => {
    return HttpService.put('/api/Department/Update', params)
        .then(response => {
            return response.data;
        })
        .catch(e => {
            alert('ex found:' + e)
        })
}
const removeDepartment = ({ commit }, data) => {
    return HttpService.delete(`/api/Department/Delete?id=${data.Id}`)
        .then(response => {
            return response.data;
        })
        .catch(e => {
            alert('ex found:' + e)
        })
}

export default {
    getDepartment,
    addDepartment,
    editDepartment,
    getDepartments,
    removeDepartment

}
