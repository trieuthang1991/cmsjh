import actions from './actions'

const _priceinlocationstore = {
    state: {
        priceinlocations: [{}],
        priceinlocation: ''
    },

    mutations: {
        SET_LOADING: (state, payload) => {
            state.isLoading = payload;
        },
        GET_PRICEINLOCATIONS: (state, payload) => {
            state.priceinlocations = payload;
        },
        GET_PRICEINLOCATION: (state, payload) => {
            state.priceinlocation = payload.Data;
        },
        UPDATE_PRICEINLOCATION: (state, payload) => {
            state.isOR = payload.data;
        },
        ADD_PRICEINLOCATION: (state, payload) => {
            state.isOR = payload.data;
        },
        DELETE_PRICEINLOCATION: (state, payload) => {
            state.isOR = payload.data;
        },
    },
    actions,
    getters: {
        priceinlocations: state => state.priceinlocations,
        priceinlocation: state => state.priceinlocation,
        total: state => state.total
    }
}
export default _priceinlocationstore;
