import HttpService from '../../../plugins/http'

const fmFileUpload = ({ commit }, data) => {
    return HttpService.post('/api/FileUploadV2/UploadImage', data, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    })
        .then(response => {
            return response.data;
        })
        .catch(e => {
            alert('ex found:' + e);
        })
}

export default {
    fmFileUpload
}
