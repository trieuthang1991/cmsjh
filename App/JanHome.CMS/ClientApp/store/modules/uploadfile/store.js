import actions from './actions'
const _uploadfilestore = {
    state: {
        isLoading: 0,
        fileName: ''
    },
    mutations: {
        SET_LOADING: (state, payload) => {
            state.isLoading = payload;
        },
        GET_FILENAME: (state, payload) => {
            state.fileName = payload;
        }
    },
    actions,
    getters: {
        fileName: state => state.fileName,
        isLoading: state => state.isLoading
    }


}
export default _uploadfilestore;
