import actions from './actions'

const _manufacturerstore = {
    state: {
        manufacturers: [{}],
        manufacturer: ''
    },

    mutations: {
        SET_LOADING: (state, payload) => {
            state.isLoading = payload;
        },
        GET_MANUFACTURERSS: (state, payload) => {
            state.manufacturers = payload;
        },
        GET_MANUFACTURERS: (state, payload) => {
            state.manufacturer = payload.Data;
        },
        UPDATE_MANUFACTURERS: (state, payload) => {
            state.isOR = payload.data;
        },
        ADD_MANUFACTURERS: (state, payload) => {
            state.isOR = payload.data;
        },
        DELETE_MANUFACTURERS: (state, payload) => {
            state.isOR = payload.data;
        },
    },
    actions,
    getters: {
        manufacturers: state => state.manufacturers,
        manufacturer: state => state.manufacturer,
        total: state => state.total
    }
}
export default _manufacturerstore;
