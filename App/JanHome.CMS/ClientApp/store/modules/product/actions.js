import HttpService from '../../../plugins/http'
import { config } from 'vue-test-utils';

const getProducts = ({ commit }, data) => {
    return HttpService.get(`/api/Product/Search?pageIndex=${data.pageIndex}&pageSize=${data.pageSize}`, {
    }).then(response => {
        commit("GET_PRODUCTS", { ...response.data })
    }).catch(e => {
        alert('ex found:' + e)
    })
}
const getProduct = ({ commit }, id) => {
    return HttpService.get(`/api/Product/GetById?id=${id}`, {
    }).then(response => {
        //console.log('ProductById'+id+response.data);
        return response.data;
    }).catch(e => {
        alert('ex found:' + e)
    })
}
const getPriceByLocation = ({ commit }, id) => {
    return HttpService.get(`/api/Product/GetPriceByLocation?idProduct=${id}`, {
    }).then(response => {
        //console.log('ProductById'+id+response.data);
        return response.data;
    }).catch(e => {
        alert('ex found:' + e)
    })
}


//const searchDataProducts = ({ commit }, data) => {
//    return HttpService.get(`/api/Product/Search?productName=${data.productName}&pageIndex=${data.pageIndex}&pageSize=${data.pageSize}&sortBy=${data.sortBy}&sortDir=${data.sortDir}`, {
//    }).then(response => {
//        commit("SEARCH_PRODUCTS", { ...response.data })
//    }).catch(e => {
//        alert('ex found:' + e)
//    })
//}

const productUnitGet = ({ commit }, data) => {
    return HttpService.get('/api/Common/ProductUnitGet')
        .then(response => {
            return response.data;
        })
        .catch(e => {
            alert('ex found:' + e)
        })
}

const updateProduct = ({ commit }, data) => {
    return HttpService.put('/api/Product/Update', data)
        .then(response => {
            return response.data;
        })
        .catch(e => {
            alert('ex found:' + e)
        })
}

const addProduct = ({ commit }, params) => {
    return HttpService.post('/api/Product/Add', params)
        .then(response => {
            return response.data;
        })
        .catch(e => {
            alert('ex found:' + e)
        })
}

const deleteProduct = ({ commit }, data) => {
    return HttpService.delete(`/api/Product/Delete?id=${data.id}`)
        .then(response => {
            return response.data;
        })
        .catch(e => {
            alert('ex found:' + e)
        })
}

export default {
    getProducts,
    getProduct,
    updateProduct,
    deleteProduct,
    addProduct,
    //searchDataProducts,
    getPriceByLocation,
    productUnitGet
}
