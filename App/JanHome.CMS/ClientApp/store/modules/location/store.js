import actions from './actions'

const _locationstore = {
    state: {
        locations: [{}],
        location: ''
    },

    mutations: {
        SET_LOADING: (state, payload) => {
            state.isLoading = payload;
        },
        GET_LOCATIONS: (state, payload) => {
            state.locations = payload;
        },
        GET_LOCATION: (state, payload) => {
            state.location = payload.Data;
        },
        UPDATE_LOCATION: (state, payload) => {
            state.isOR = payload.data;
        },
        ADD_LOCATION: (state, payload) => {
            state.isOR = payload.data;
        },
        DELETE_LOCATION: (state, payload) => {
            state.isOR = payload.data;
        },
    },
    actions,
    getters: {
        locations: state => state.locations,
        location: state => state.location,
        total: state => state.total
    }
}
export default _locationstore;
