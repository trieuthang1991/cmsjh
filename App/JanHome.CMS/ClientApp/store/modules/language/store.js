import actions from './actions'

const _languagestore = {
    state: {
        languages: [{}],
        language: ''
    },

    mutations: {
        SET_LOADING: (state, payload) => {
            state.isLoading = payload;
        },
        GET_LANGUAGES: (state, payload) => {
            state.languages = payload;
        },
        GET_LANGUAGE: (state, payload) => {
            state.language = payload.Data;
        },
        UPDATE_LANGUAGE: (state, payload) => {
            state.isOR = payload.data;
        },
        ADD_LANGUAGE: (state, payload) => {
            state.isOR = payload.data;
        },
        DELETE_LANGUAGE: (state, payload) => {
            state.isOR = payload.data;
        },
    },
    actions,
    getters: {
        languages: state => state.languages,
        language: state => state.language,
        total: state => state.total
    }
}
export default _languagestore;
