import HttpService from '../../../plugins/http'
import { config } from 'vue-test-utils';

const getConfigs = ({ commit }, data) => {
    return HttpService.get(`/api/Config/Get?pageIndex=${data.pageIndex}&pageSize=${data.pageSize}&sortBy=${data.sortBy}&sortDir=${data.sortDir}`, {
    }).then(response => {
        commit("GET_CONFIGS", { ...response.data })
    }).catch(e => {
        alert('ex found:' + e)
    })
}
const getConfig = ({ commit }, id) => {
    return HttpService.get(`/api/Config/GetById?id=${id}`, {
    }).then(response => {
        return response.data;
    }).catch(e => {
        alert('ex found:' + e)
    })
}

const updateConfig = ({ commit }, data) => {
    return HttpService.put('/api/Config/Update', data)
        .then(response => {
            return response.data;
        })
        .catch(e => {
            alert('ex found:' + e)
        })
}

const addConfig = ({ commit }, params) => {
    return HttpService.put('/api/Config/Add', params)
        .then(response => {
            return response.data;
        })
        .catch(e => {
            alert('ex found:' + e)
        })
}

const deleteConfig = ({ commit }, data) => {
    return HttpService.delete(`/api/Config/Delete?id=${data.Id}`)
        .then(response => {
            return response.data;
        })
        .catch(e => {
            alert('ex found:' + e)
        })
}

export default {
    getConfigs,
    getConfig,
    updateConfig,
    deleteConfig,
    addConfig
}
