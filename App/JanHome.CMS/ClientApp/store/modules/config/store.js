import actions from './actions'

const _configstore = {
    state: {
        configs: [{}],
        config: ''
    },

    mutations: {
        SET_LOADING: (state, payload) => {
            state.isLoading = payload;
        },
        GET_CONFIGS: (state, payload) => {
            state.configs = payload;
        },
        GET_CONFIG: (state, payload) => {
            state.config = payload.Data;
        },
        UPDATE_CONFIG: (state, payload) => {
            state.isOR = payload.data;
        },
        ADD_CONFIG: (state, payload) => {
            state.isOR = payload.data;
        },
        DELETE_CONFIG: (state, payload) => {
            state.isOR = payload.data;
        },
    },
    actions,
    getters: {
        configs: state => state.configs,
        config: state => state.config,
        total: state => state.total
    }
}
export default _configstore;
