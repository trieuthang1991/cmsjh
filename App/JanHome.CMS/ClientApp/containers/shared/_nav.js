export default {
    items: [
        {
            name: 'Tổng quan',
            url: '/dashboard',
            icon: 'icon-speedometer',
            badge: {
                variant: 'primary',
                // text: 'NEW'
            }
        },
        {
            name: 'Sản phẩm',
            url: '/product',
            icon: 'icon-drop',
            children: [
                {
                    name: 'Danh sách',
                    url: '/product/list',
                    icon: 'icon-puzzle'
                },
                {
                    name: 'Vùng hiển thị',
                    url: '/product/list',
                    icon: 'icon-puzzle'
                },
                {
                    name: 'Thuộc tính',
                    url: '/product/property',
                    icon: 'icon-puzzle'
                },
                {
                    name: 'Khuyến mãi',
                    url: '/promotion/list',
                    icon: 'icon-puzzle'
                }
            ]
        },
        {
            name: 'Bài viết',
            url: '/theme/typography',
            icon: 'icon-pencil'
        }, {
            name: 'Đơn hàng',
            url: '/theme/typography',
            icon: 'icon-pencil'
        },
        {
            name: 'Khách hàng',
            url: '/config/list',
            icon: 'icon-puzzle',

        },
        {
            name: 'Bài viết',
            url: '/article/list',
            icon: 'icon-puzzle'

        },
        {
            name: 'Danh sách nhà cung cấp',
            url: '/manufacturers/list',
            icon: 'icon-puzzle',
        },
        {
            name: 'Cấu hình',
            url: '/ads',
            icon: 'icon-drop',
            children: [
                {
                    name: 'Thông tin website',
                    url: '/ads/list',
                    icon: 'icon-puzzle',

                }, {
                    name: 'Quảng cáo',
                    url: '/ads/list',
                    icon: 'icon-puzzle',

                },
                {
                    name: 'Phòng ban',
                    url: '/department/list',
                    icon: 'icon-puzzle',

                },
                {
                    name: 'Tags',
                    url: '/tags/list',
                    icon: 'icon-puzzle',

                },
                {
                    name: 'Danh mục',
                    url: '/zone/list',
                    icon: 'icon-puzzle',

                },
                {
                    name: 'Ngôn ngữ',
                    url: '/language/list',
                    icon: 'icon-puzzle',

                },
                {
                    name: 'Vị trí',
                    url: '/location/list',
                    icon: 'icon-puzzle',

                }
            ]
        }

    ]
}
