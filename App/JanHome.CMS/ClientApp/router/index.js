import Vue from 'vue'
import Router from 'vue-router'
import { authenticationRepository } from "../repository/authentication/authenticationRepository";
// home
const DefaultContainer = () => import('./../containers/DefaultContainer');
const Dashboard = () => import('./../pages/Dashboard');




const ProductExtent = () => import('./../pages/productextend/main')

const Products = () => import('./../pages/product/list');
const ProductEdit = () => import('./../pages/product/edit');
const ProductAdd = () => import('./../pages/product/add');

const Promotions = () => import('./../pages/promotion/list');
const PromotionEdit = () => import('./../pages/promotion/edit');


const Forms = () => import('./../pages/Forms');
const Page404 = () => import('./../pages/Page404');
const Login = () => import('./../pages/Login');

const Config = () => import('./../pages/configs/list');
const ConfigEdit = () => import('./../pages/configs/edit');


const Zone = () => import('./../pages/zone/list');
const ZoneEdit = () => import('./../pages/zone/edit');


const Location = () => import('./../pages/location/list');
const LocationEdit = () => import('./../pages/location/edit');

const Language = () => import('./../pages/language/list');
const LanguageEdit = () => import('./../pages/language/edit');

const Ads = () => import('./../pages/ads/list');
const AdsEdit = () => import('./../pages/ads/edit');

const DepartmentEdit = () => import('./../pages/department/edit');
const Department = () => import('./../pages/department/list');
//Tag
const Tag = () => import('./../pages/tags/list');
const TagEdit = () => import('./../pages/tags/edit');

const Article = () => import('./../pages/Article/list');
const ArticleEdit = () => import('./../pages/Article/edit');


const Manufacturers = () => import('./../pages/manufacturer/list');
const ManufacturersEdit = () => import('./../pages/manufacturer/edit');

Vue.use(Router)

//export function createRouter() {
//    return new Router({
//        mode: 'history',
//        hashbang: false,
//        history: true,
//        linkActiveClass: "active",
//        routes: [
//            {
//                path: '/',
//                redirect: '/dashboard',
//                component: DefaultContainer,
//                display: 'Home',
//                style: 'glyphicon glyphicon-home',
//                children: [
//                    {
//                        path: 'dashboard',
//                        name: 'Tổng quan',
//                        component: Dashboard

//                    },

//                    {
//                        path: 'product/list',
//                        name: 'Danh sách sản phẩm',
//                        component: Products
//                    },
//                    {
//                        path: 'product/add',
//                        name: 'Thêm mới sản phẩm',
//                        component: ProductEdit
//                    },
//                    {
//                        path: 'product/edit/:id',
//                        name: 'Sửa sản phẩm',
//                        component: ProductEdit
//                    },
//                    {
//                        path: 'product/productextent/:id',
//                        name: 'Chức năng mở rộng',
//                        component: ProductExtent
//                    },
//                    {
//                        path: 'config/list',
//                        name: 'Cấu hình',
//                        component: Config
//                    },
//                    {
//                        path: 'config/add',
//                        name: 'Thêm mới cấu hình',
//                        component: ConfigEdit
//                    },
//                    {
//                        path: 'config/edit/:id',
//                        name: 'Sửa cấu hình',
//                        component: ConfigEdit
//                    },


//                    {
//                        path: '/zone/list',
//                        name: 'Nhóm bài viết',
//                        component: Zone
//                    },
//                    {
//                        path: '/zone/add',
//                        name: 'Thêm mới nhóm bài viết',
//                        component: ZoneEdit
//                    },
//                    {
//                        path: '/zone/edit/:id',
//                        name: 'Sửa nhóm bài viết',
//                        component: ZoneEdit
//                    },


//                    {
//                        path: 'location/list',
//                        name: 'Vị trí',
//                        component: Location
//                    },
//                    {
//                        path: 'location/add',
//                        name: 'Thêm mới vị trí',
//                        component: LocationEdit
//                    },
//                    {
//                        path: 'location/edit/:id',
//                        name: 'Sửa vị trí',
//                        component: LocationEdit
//                    },

//                    {
//                        path: '/language/list',
//                        name: 'Ngôn ngữ',
//                        component: Language
//                    },
//                    {
//                        path: '/language/add',
//                        name: 'Thêm ngôn ngữ',
//                        component: LanguageEdit
//                    },
//                    {
//                        path: '/language/edit/:id',
//                        name: 'Sửa ngôn ngữ',
//                        component: LanguageEdit
//                    },

//                    {
//                        path: 'ads/list',
//                        name: 'Quảng cáo',
//                        component: Ads
//                    },
//                    {
//                        path: 'ads/add',
//                        name: 'Thêm mới quảng cáo',
//                        component: AdsEdit
//                    },
//                    {
//                        path: 'ads/edit/:id',
//                        name: 'Sửa quảng cáo',
//                        component: AdsEdit
//                    },
//                    {
//                        path: 'department/list',
//                        name: 'Danh sách phòng',
//                        component: Department
//                    },
//                    {
//                        path: 'department/add',
//                        name: 'Thêm mới phòng',
//                        component: DepartmentEdit
//                    },
//                    {
//                        path: 'department/edit/:id',
//                        name: 'Sửa phòng',
//                        component: DepartmentEdit
//                    },
//                    {
//                        path: 'tags/list',
//                        name: 'Danh sách Tags',
//                        component: Tag
//                    },
//                    {
//                        path: 'tags/add',
//                        name: 'Thêm mới Tags',
//                        component: TagEdit
//                    },
//                    {
//                        path: 'tags/edit/:id',
//                        name: 'Sửa Tags',
//                        component: TagEdit
//                    },
//                    {
//                        path: 'article/list',
//                        name: 'Danh sách bài viết',
//                        component: Article
//                    },
//                    {
//                        path: 'article/add',
//                        name: 'Thêm mới bài viết',
//                        component: ArticleEdit
//                    },
//                    {
//                        path: 'article/edit/:id',
//                        name: 'Sửa bài viết',
//                        component: ArticleEdit
//                    },
//                ]

//            }
//            })
//};


export let router = new Router({
    mode: 'history',
    hashbang: false,
    history: true,
    linkActiveClass: "active",
    routes: [
        {
            path: '/',
            redirect: '/dashboard',
            component: DefaultContainer,
            display: 'Home',
            style: 'glyphicon glyphicon-home',
            children: [
                {
                    path: 'dashboard',
                    name: 'Tổng quan',
                    component: Dashboard,
                    meta: { authorize: [] }
                },

                {
                    path: 'product/list',
                    name: 'Danh sách sản phẩm',
                    component: Products
                },
                {
                    path: 'product/add',
                    name: 'Thêm mới sản phẩm',
                    component: ProductAdd
                },
                {
                    path: 'product/edit/:id',
                    name: 'Sửa sản phẩm',
                    component: ProductEdit
                },
                {
                    path: 'product/productextent/:id',
                    name: 'Phần mở rộng',
                    component: ProductExtent
                },

                {
                    path: 'config/list',
                    name: 'Cấu hình',
                    component: Config
                },
                {
                    path: 'config/add',
                    name: 'Thêm mới cấu hình',
                    component: ConfigEdit
                },
                {
                    path: 'config/edit/:id',
                    name: 'Sửa cấu hình',
                    component: ConfigEdit
                },


                {
                    path: '/zone/list',
                    name: 'Nhóm bài viết',
                    component: Zone
                },
                {
                    path: '/zone/add',
                    name: 'Thêm mới nhóm bài viết',
                    component: ZoneEdit
                },
                {
                    path: '/zone/edit/:id',
                    name: 'Sửa nhóm bài viết',
                    component: ZoneEdit
                },


                {
                    path: 'location/list',
                    name: 'Vị trí',
                    component: Location
                },
                {
                    path: 'location/add',
                    name: 'Thêm mới vị trí',
                    component: LocationEdit
                },
                {
                    path: 'location/edit/:id',
                    name: 'Sửa vị trí',
                    component: LocationEdit
                },

                {
                    path: '/language/list',
                    name: 'Ngôn ngữ',
                    component: Language
                },
                {
                    path: '/language/add',
                    name: 'Thêm ngôn ngữ',
                    component: LanguageEdit
                },
                {
                    path: '/language/edit/:id',
                    name: 'Sửa ngôn ngữ',
                    component: LanguageEdit
                },

                {
                    path: 'ads/list',
                    name: 'Quảng cáo',
                    component: Ads
                },
                {
                    path: 'ads/add',
                    name: 'Thêm mới quảng cáo',
                    component: AdsEdit
                },
                {
                    path: 'ads/edit/:id',
                    name: 'Sửa quảng cáo',
                    component: AdsEdit
                },
                {
                    path: 'department/list',
                    name: 'Danh sách phòng',
                    component: Department
                },
                {
                    path: 'department/add',
                    name: 'Thêm mới phòng',
                    component: DepartmentEdit
                },
                {
                    path: 'department/edit/:id',
                    name: 'Sửa phòng',
                    component: DepartmentEdit
                },
                {
                    path: 'tags/list',
                    name: 'Danh sách Tags',
                    component: Tag
                },
                {
                    path: 'tags/add',
                    name: 'Thêm mới Tags',
                    component: TagEdit
                },
                {
                    path: 'tags/edit/:id',
                    name: 'Sửa Tags',
                    component: TagEdit
                },
                {
                    path: 'article/list',
                    name: 'Danh sách bài viết',
                    component: Article
                },
                {
                    path: 'article/add',
                    name: 'Thêm mới bài viết',
                    component: ArticleEdit
                },
                {
                    path: 'article/edit/:id',
                    name: 'Sửa bài viết',
                    component: ArticleEdit
                },
                {
                    path: 'manufacturers/list',
                    name: 'Danh sách nhà cung cấp',
                    component: Manufacturers
                },
                {
                    path: 'manufacturers/add',
                    name: 'Thêm mới nhà cung cấp',
                    component: ManufacturersEdit
                },
                {
                    path: 'manufacturers/edit/:id',
                    name: 'Sửa nhà cung cấp',
                    component: ManufacturersEdit
                },
                {
                    path: 'promotion/list',
                    name: 'Danh sách khuyến mãi',
                    component: Promotions
                },
                {
                    path: 'promotion/add',
                    name: 'Thêm mới khuyến mãi',
                    component: PromotionEdit
                },
                {
                    path: 'promotion/edit/:id',
                    name: 'Sửa khuyến mãi',
                    component: PromotionEdit
                }
            ]

        },
        //{
        //    path: 'cau-hinh',
        //    redirect: 'cau-hinh/config',
        //    display: 'Setting',
        //    component: {
        //        render(c) { return c('router-view') }
        //    },
        //    children: [
        //        {
        //            path: 'config',
        //            name: 'Config',
        //            component: Config
        //        },

        //    ]
        //},
        {
            path: '/pages',
            redirect: '/pages/404',
            name: 'Pages',
            component: {
                render(c) {
                    return c('router-view')
                }
            },
            children: [
                {
                    path: '404',
                    name: 'Page404',
                    component: Page404
                },

            ]
        },
        {
            path: '/login',
            name: 'Login',
            component: Login
        }
    ]
});

//router.beforeEach((to, from, next) => {
//    // redirect to login page if not logged in and trying to access a restricted page
//    const { authorize } = to.meta;
//    const currentUser = authenticationRepository.currentUserValue;

//    if (authorize) {
//        if (!currentUser) {
//            // not logged in so redirect to login page with the return url
//            return next({ path: '/login', query: { returnUrl: to.path } });
//        }
//        // check if route is restricted by role
//        // if (authorize.length && !authorize.includes(currentUser.role)) {
//        //     // role not authorised so redirect to home page
//        //     return next({path: '/'});
//        // }
//    }
//    next();
//});


