namespace MI.Dapper.Data.ViewModels
{
    public class ArticleInLanguageViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Body { get; set; }
        public string Author { get; set; }
        public int? WordCount { get; set; }
        public int? ViewCount { get; set; }
        public string Url { get; set; }
        public string MetaKeyword { get; set; }
        public string MetaDescription { get; set; }
        public string MetaTitle { get; set; }
        public bool? IsAllowComment { get; set; }
        public string SocialTitle { get; set; }
        public string SocialDescription { get; set; }
        public string SocialImage { get; set; }
        public string LanguageCode { get; set; }
        public int ArticleId { get; set; }
    }
}