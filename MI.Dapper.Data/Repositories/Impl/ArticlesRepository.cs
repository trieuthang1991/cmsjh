﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using MI.Dapper.Data.Helpers;
using MI.Dapper.Data.Models;
using MI.Dapper.Data.Repositories.Interfaces;
using MI.Dapper.Data.ViewModels;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Utils;

namespace MI.Dapper.Data.Repositories.Impl
{
    public class ArticlesRepository : IArticlesRepository
    {
        private readonly string _connectionString;
        private readonly ILogger<ArticlesRepository> _logger;

        public ArticlesRepository(IConfiguration configuration,
            ILogger<ArticlesRepository> logger)
        {
            _logger = logger;
            _connectionString = configuration.GetConnectionString("DefaultConnection");
        }

        public async Task<int> Create(Articles articles)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                try
                {
                    if (connection.State == ConnectionState.Closed)
                    {
                        connection.Open();
                    }

                    var parameters = new DynamicParameters();
                    parameters.Add("@Avatar", articles.Avatar);
                    parameters.Add("@Status ", articles.Status);
                    parameters.Add("@Type ", articles.Type);
                    parameters.Add("@LastModifiedDate ", articles.LastModifiedDate);
                    parameters.Add("@DistributionDate ", articles.DistributionDate);
                    parameters.Add("@CreatedBy ", articles.CreatedBy);
                    parameters.Add("@LastModifiedBy ", articles.LastModifiedBy);
                    parameters.Add("@PublishedBy ", articles.PublishedBy);
                    parameters.Add("@Url ", articles.Url);
                    parameters.Add("@ZoneIds ", articles.ZoneIds);
                    parameters.Add("@ProductIds ", articles.ProductIds);
                    parameters.Add("@Title ", articles.Title);
                    parameters.Add("@Description", articles.Description);
                    parameters.Add("@Body", articles.Body);
                    parameters.Add("@Author", articles.Author);
                    parameters.Add("@WordCount", articles.WordCount);
                    parameters.Add("@MetaTitle ", articles.MetaTitle);
                    parameters.Add("@MetaKeyword", articles.MetaKeyword);
                    parameters.Add("@MetaDescription ", articles.MetaDescription);
                    parameters.Add("@SocialTitle ", articles.SocialTitle);
                    parameters.Add("@SocialDescription", articles.SocialDescription);
                    parameters.Add("@SocialImage ", articles.SocialImage);
                    parameters.Add("@LanguageCode ", articles.LanguageCode);
                    parameters.Add("@id", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    var result = await connection.ExecuteAsync("Create_Articles", parameters, null, null,
                        CommandType.StoredProcedure);
                    var newId = parameters.Get<int>("@id");
                    return newId;
                }
                catch (Exception e)
                {
                }
            }

            return 0;
        }

        public async Task<int> Update(Articles article)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                try
                {
                    if (connection.State == ConnectionState.Closed)
                    {
                        connection.Open();
                    }

                    var parameters = new DynamicParameters();
                    parameters.Add("@id", article.Id);
                    parameters.Add("@Avatar", article.Avatar);
                    parameters.Add("@Status ", article.Status);
                    parameters.Add("@Type ", article.Type);
                    parameters.Add("@LastModifiedDate ", article.LastModifiedDate);
                    parameters.Add("@DistributionDate ", article.DistributionDate);
                    parameters.Add("@CreatedBy ", article.CreatedBy);
                    parameters.Add("@LastModifiedBy ", article.LastModifiedBy);
                    parameters.Add("@PublishedBy ", article.PublishedBy);
                    parameters.Add("@Url ", article.Url);
                    parameters.Add("@ZoneIds ", article.ZoneIds);
                    parameters.Add("@ProductIds ", article.ProductIds);
                    parameters.Add("@Title ", article.Title);
                    parameters.Add("@Description", article.Description);
                    parameters.Add("@Body", article.Body);
                    parameters.Add("@Author", article.Author);
                    parameters.Add("@WordCount", article.WordCount);
                    parameters.Add("@MetaTitle ", article.MetaTitle);
                    parameters.Add("@MetaKeyword", article.MetaKeyword);
                    parameters.Add("@MetaDescription ", article.MetaDescription);
                    parameters.Add("@SocialTitle ", article.SocialTitle);
                    parameters.Add("@SocialDescription", article.SocialDescription);
                    parameters.Add("@SocialImage ", article.SocialImage);
                    parameters.Add("@LanguageCode ", article.LanguageCode);
                    var result = await connection.ExecuteAsync("Update_Articles", parameters, null, null,
                        CommandType.StoredProcedure);
                    return article.Id;
                }
                catch (Exception e)
                {
                }
            }

            return 0;
        }

        public async Task<ArticlesViewModel> GetById(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                try
                {
                    var sqlGetById = "select * from article where id=" + id;
                    var sqlZoneIdByArticleId =
                        "select ZoneId from ArticlesInZone aiz inner join article a on a.id= aiz.articleid where a.id=" +
                        id;
                    var sqlProductIdByArticleId =
                        "select pia.productid as id,Title as name from ProductInLanguage p inner join ProductInArticle pia on p.productid = pia.productid where p.LanguageCode='vi-VN' and pia.ArticleId=" +
                        id;
                    var sqlArticleInLanguage = "select * from articleinlanguage where ArticleId=" + id;
                    var resultGetById = await
                        connection.QueryAsync<Articles>(sqlGetById, null, null, null, CommandType.Text);
                    var resultGetZoneByArticleId =
                        await connection.QueryAsync<int>(sqlZoneIdByArticleId, null, null, null,
                            CommandType.Text);
                    var resultGetProductIdByArticleId = await
                        connection.QueryAsync<ProductAtArticle>(sqlProductIdByArticleId, null, null, null,
                            CommandType.Text);
                    var resultArticleInLanguage = await connection.QueryAsync<ArticleInLanguageViewModel>(
                        sqlArticleInLanguage, null, null, null,
                        CommandType.Text);
                    var articleGetById = new ArticlesViewModel();
                    articleGetById.Articles = resultGetById.SingleOrDefault();
                    articleGetById.ListZoneIds = resultGetZoneByArticleId.ToList();
                    articleGetById.ProductAtArticles = resultGetProductIdByArticleId.ToList();
                    articleGetById.ArticleInLanguageViewModels = resultArticleInLanguage.ToList();
                    return articleGetById;
                }
                catch (Exception ex)
                {
                }
            }

            return null;
        }

        public async Task<PagedResult<ArticlePagingViewModel>> GetAllPaging(FilterArticle article)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                try
                {
                    if (connection.State == ConnectionState.Closed)
                    {
                        connection.Open();
                    }

                    // var parameters = new DynamicParameters();
                    // parameters.Add("@keyword", article.keyword);
                    // parameters.Add("@pageIndex", article.pageIndex);
                    // parameters.Add("@pageSize", article.pageSize);
                    // parameters.Add("@zoneIds", article.ZoneIds);
                    // parameters.Add("@status", article.Status);
                    // parameters.Add("@type", article.Type);
                    // parameters.Add("@totalRow", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    // var result = await connection.QueryAsync<ArticlePagingViewModel>("Get_Article_AllPaging",
                    //     parameters, null, null,
                    //     CommandType.StoredProcedure);
                    // var totalRow = parameters.Get<int>("@totalRow");
                    var sql = BuildQuerySearch.CountTotalAndGetSearchRecord(article);
                    var result =  connection.QueryMultiple(sql);

                    // var articlesPaging = new PagedResult<ArticlePagingViewModel>()
                    // {
                    //     Items = result.ToList(),
                    //     TotalRow = totalRow,
                    //     PageIndex = article.pageIndex,
                    //     PageSize = article.pageSize
                    // };
                    var articlesPaging = new PagedResult<ArticlePagingViewModel>()
                    {
                      
                        TotalRow = result.Read<int>().Single(),
                        Items = result.Read<ArticlePagingViewModel>().ToList(),
                        PageIndex = article.pageIndex,
                        PageSize = article.pageSize
                    };
                    return articlesPaging;
                }
                catch (Exception e)
                {
                }
            }

            return new PagedResult<ArticlePagingViewModel>();
        }
    }
}