﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MI.Dapper.Data.Models;
using MI.Dapper.Data.ViewModels;
using Utils;

namespace MI.Dapper.Data.Repositories.Interfaces
{
    public interface IArticlesRepository
    {
        Task<int> Create(Articles article);
        Task<int> Update(Articles article);
        Task<ArticlesViewModel> GetById(int id);
        Task<PagedResult<ArticlePagingViewModel>> GetAllPaging(FilterArticle article);
    }
}