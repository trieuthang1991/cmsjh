using System.Collections.Generic;
using System.Threading.Tasks;

namespace MI.Dapper.Data.Repositories.Interfaces
{
    public interface IPermissionRepository
    {
        Task<List<string>> GetPermissionByUserId(string userId);
    }
}