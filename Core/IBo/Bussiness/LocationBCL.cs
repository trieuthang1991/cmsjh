﻿using MI.Dal.IDbContext;
using MI.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Utils;

namespace MI.Bo.Bussiness
{
    public partial class LocationBCL : Base<Location>
    {
        public LocationBCL()
        {

        }
        public List<Location> Get(FilterQuery filter, out int total)
        {
            using (IDbContext _context = new IDbContext())
            {
                var Location = _context.Location.AsQueryable();

                if (!String.IsNullOrEmpty(filter.keyword))
                {
                    Location = Location.Where(x => x.Id.ToString().Contains(filter.keyword) || x.Name.ToLower().ToString().Contains(filter.keyword.ToLower()));
                }
                if (filter.sortDir == "asc")
                {
                    Location = Location.OrderBy(x => x.GetType().GetProperty(filter.sortBy).GetValue(x, null));
                }
                else
                {
                    Location = Location.OrderByDescending(x => x.GetType().GetProperty(filter.sortBy).GetValue(x, null));
                }

                total = Location.Count();

                return Location.Skip((filter.pageIndex - 1) * filter.pageSize).Take(filter.pageSize).ToList();
            }
        }
    }
}
