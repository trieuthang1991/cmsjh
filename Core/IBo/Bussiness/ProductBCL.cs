﻿using MI.Dal.IDbContext;
using MI.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MI.Bo.Bussiness
{
    public partial class ProductBCL : Base<Product>
    {
        public ProductBCL()
        {

        }
        public List<Product> Get(int pageIndex, int pageSize, string sortBy, string sortDir, out int total)
        {
            using (IDbContext _context = new IDbContext())
            {
                var Product = _context.Product.AsQueryable();
                if (sortDir == "asc")
                {
                    Product = Product.OrderBy(x => x.GetType().GetProperty(sortBy).GetValue(x, null));
                }
                else
                {
                    Product = Product.OrderByDescending(x => x.GetType().GetProperty(sortBy).GetValue(x, null));
                }

                total = Product.Count();

                return Product.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
            }
        }
        public List<Product> Search(string productName, int pageIndex, int pageSize, string sortBy, string sortDir, out int total)
        {
            using (IDbContext _context = new IDbContext())
            {
                var Product = _context.Product.AsQueryable();

                if (sortDir == "asc")
                {
                    Product = Product.OrderBy(x => x.GetType().GetProperty(sortBy).GetValue(x, null));
                }
                else
                {
                    Product = Product.OrderByDescending(x => x.GetType().GetProperty(sortBy).GetValue(x, null));
                }

                total = Product.Count();

                return Product.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
            }
        }

        public int CreateProduct(Product product)
        {
            int result = 0;
            try
            {
                using (IDbContext db = new IDbContext())
                {

                    var kH = db.Product.FirstOrDefault(n => n.Code.Trim() == product.Code.Trim());
                    if (kH == null)
                    {
                        db.Set<Product>().Add(product);

                        db.SaveChanges();
                        result = product.Id;
                    }
                    else
                        result = -1; // Sản phẩm đã tồn tại
                }

            }
            catch (Exception ex)
            {
                return -99;
            }
            return result;
        }
        public bool UpdateTrangThai(Product entity)
        {
            try
            {
                using (IDbContext _context = new IDbContext())
                {
                    _context.Attach(entity);
                    var entry = _context.Entry(entity);
                    entry.Property(p => p.Status).IsModified = true;
                    _context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {

            }
            return false;
        }

    }
}
