﻿using MI.Dal.IDbContext;
using MI.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Utils;

namespace MI.Bo.Bussiness
{
    public partial class ArticleBCL : Base<Article>
    {
        public ArticleBCL()
        {

        }
        public List<Article> GetPage(FilterArticle filter, out int total)
        {
            using (var context = new IDbContext())
            {
                var articles = context.Article.AsQueryable();
                total = articles.Count();
                if (!(filter.Status == null || filter.Status == 0))
                    articles = articles.Where(x => x.Status == filter.Status);
                if (!string.IsNullOrEmpty(filter.keyword))
                    //articles = articles.Where(x => x.t.Contains(filter.keyword));
                if (!(filter.Type == null || filter.Type == 0))
                    articles = articles.Where(x => x.Type == filter.Type);
                return articles.Skip((filter.pageIndex - 1) * filter.pageSize).Take(filter.pageSize).ToList();
            }
        }
    }
}
