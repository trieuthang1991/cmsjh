﻿using MI.Dal.IDbContext;
using MI.Entity.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils;

namespace MI.Bo.Bussiness
{
    public class ProductInLanguageBCL : Base<ProductInLanguage>
    {
        IDbContext _context = new IDbContext();
        public ProductInLanguageBCL()
        {

        }
        public List<ProductInLanguage> Get(string keyword, int pageIndex, int pageSize, string sortBy, string sortDir, out int total)
        {
            //var result = _context.ProductInLanguage.AsQueryable();

            //if (!string.IsNullOrEmpty(keyword))
            //    result = result.Where(r => r.Title.ToLower().Contains(keyword.ToLower()));
            //if (sortDir == "asc")
            //{
            //    result = result.OrderBy(x => x.GetType().GetProperty(sortBy).GetValue(x, null));
            //}
            //else
            //{
            //    result = result.OrderByDescending(x => x.GetType().GetProperty(sortBy).GetValue(x, null));
            //}
            total = 0;

            return new List<ProductInLanguage>();
        }
        public List<ProductInLanguage> FindAll(FilterProduct filter, out int total)
        {
            total = 0;
            try
            {
                using (IDbContext _context = new IDbContext())
                {
                    IQueryable<ProductInLanguage> items = _context.ProductInLanguage.AsQueryable();
                    items = items.Include(x => x.Product).ThenInclude(x => x.ProductInZone);

                    items = items.Where(x => x.LanguageCode.Trim().Equals(filter.languageCode.Trim()));

                    if (!String.IsNullOrEmpty(filter.keyword))
                    {
                        items = items.Where(x => x.Title.Contains(filter.keyword) || x.ProductId.ToString() == filter.keyword);
                    }
                    if (filter.trangThai != Entity.Enums.StatusProduct.All)
                    {
                        items = items.Where(x => x.Product.Status == (byte)filter.trangThai);
                    }
                    else
                    {
                        if (filter.trangThai != Entity.Enums.StatusProduct.Deleted)
                        {
                            items = items.Where(x => x.Product.Status != (byte)Entity.Enums.StatusProduct.Deleted);
                        }
                    }
                    if (filter.idZone > 0)
                    {
                        items = items.Where(x => x.Product.ProductInZone.Any(d => d.ZoneId == filter.idZone));
                    }

                    total = items.Count();
                    filter.pageIndex = filter.pageIndex < 1 ? 1 : filter.pageIndex;
                    return items.OrderBy(x => x.Product.CreatedDate).Skip((filter.pageIndex - 1) * filter.pageSize).Take(filter.pageSize).Select(x => new { x.ProductId, x.Product.Price, x.Title, x.Product.Avatar, x.Product.ProductInZone }).ToList().Select(d => new ProductInLanguage { ProductId = d.ProductId, Title = d.Title, Product = new Product { Avatar = d.Avatar, ProductInZone = d.ProductInZone, Price = d.Price } }).ToList();
                }
            }
            catch (Exception ex)
            {

            }
            return new List<ProductInLanguage>();
        }

        public List<ProductInLanguage> FindByListId(List<int> lstId, string languageCode = "vi-VN")
        {

            try
            {
                using (IDbContext _context = new IDbContext())
                {
                    IQueryable<ProductInLanguage> items = _context.ProductInLanguage.AsQueryable();
                    items = items.Include(x => x.Product);

                    return items.Where(x => x.LanguageCode.Trim() == languageCode && lstId.Contains(x.ProductId)).Select(x => new { x.ProductId, x.Product.Price, x.Title, x.Product.Avatar }).ToList().Select(d => new ProductInLanguage { ProductId = d.ProductId, Title = d.Title, Product = new Product { Avatar = d.Avatar, Price = d.Price } }).ToList();
                }
            }
            catch (Exception ex)
            {

            }
            return new List<ProductInLanguage>();
        }

        public List<ProductInLanguage> GetByProductId(int productId, string languageCode)
        {
            List<ProductInLanguage> productInLanguages = new List<ProductInLanguage>();
            try
            {
                productInLanguages = _context.ProductInLanguage.Where(n => n.ProductId == productId && n.LanguageCode == languageCode).ToList();
            }
            catch (Exception ex)
            {

            }
            return productInLanguages;
        }
    }
}
