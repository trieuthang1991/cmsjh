﻿

﻿using MI.Dal.IDbContext;
using MI.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;

using System.Text;

namespace MI.Bo.Bussiness
{
    public class PromotionBCL : Base<Promotion>
    {
        public PromotionBCL()
        {

        }

        public List<Promotion> Get(int pageIndex, int pageSize, string sortBy, string sortDir, out int total)
        {
            using (IDbContext _context = new IDbContext())
            {
                var Promotion = _context.Promotion.AsQueryable();
                if (sortDir == "asc")
                {
                    Promotion = Promotion.OrderBy(x => x.GetType().GetProperty(sortBy).GetValue(x, null));
                }
                else
                {
                    Promotion = Promotion.OrderByDescending(x => x.GetType().GetProperty(sortBy).GetValue(x, null));
                }

                total = Promotion.Count();

                return Promotion.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
            }
        }

    }
}
