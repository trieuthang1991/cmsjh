﻿using MI.Dal.IDbContext;
using MI.Entity.Models;
using EFCore.BulkExtensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace MI.Bo.Bussiness
{
    public class ZoneInLanguageBCL : Base<ZoneInLanguage>
    {
        public ZoneInLanguageBCL()
        {

        }
        public List<ZoneInLanguage> FindAll(string tuKhoa, MI.Entity.Enums.StatusZone trangThai, MI.Entity.Enums.TypeZone loai, string languageCode = "vi-VN")
        {
            try
            {
                using (IDbContext _context = new IDbContext())
                {
                    IQueryable<ZoneInLanguage> items = _context.ZoneInLanguage.AsQueryable();
                    items = items.Where(x => x.LanguageCode.Trim().Equals(languageCode));
                    if (!String.IsNullOrEmpty(tuKhoa))
                    {
                        items = items.Where(x => x.Name.Contains(tuKhoa) || x.ZoneId.ToString() == tuKhoa);
                    }
                    if (trangThai != Entity.Enums.StatusZone.All)
                    {
                        items = items.Where(x => x.Zone.Status == (byte)trangThai);
                    }
                    else
                    {
                        if (trangThai != Entity.Enums.StatusZone.Delete)
                        {
                            items = items.Where(x => x.Zone.Status != (byte)Entity.Enums.StatusZone.Delete);
                        }
                    }
                    if (loai != Entity.Enums.TypeZone.All)
                    {
                        items = items.Where(x => x.Zone.Type == (byte)loai);
                    }


                    items = items.Include(x => x.Zone);
                    return items.Select(x => new { x.ZoneId, x.Name, x.Zone.ParentId, x.Zone.SortOrder }).OrderBy(x => x.SortOrder).ToList().Select(d => new ZoneInLanguage { ZoneId = d.ZoneId, Name = d.Name, Zone = new Zone { ParentId = d.ParentId, SortOrder = d.SortOrder } }).ToList();
                }
            }
            catch (Exception ex)
            {

            }
            return new List<ZoneInLanguage>();
        }

        public List<ZoneInLanguage> GetById(List<int> lstId, string languageCode = "vn")
        {
            try
            {
                using (IDbContext _context = new IDbContext())
                {
                    return _context.ZoneInLanguage.Where(x => lstId.Contains(x.ZoneId) && x.LanguageCode.Trim().Equals(languageCode)).Select(x => new { x.ZoneId, x.Name }).ToList().Select(x => new ZoneInLanguage { ZoneId = x.ZoneId, Name = x.Name }).ToList();



                }
            }
            catch (Exception ex)
            {

            }
            return new List<ZoneInLanguage>();
        }
        public bool Merge(ZoneInLanguage entity)
        {
            try
            {
                using (IDbContext _context = new IDbContext())
                {
                    _context.BulkInsertOrUpdate<ZoneInLanguage>(new List<ZoneInLanguage> { entity });
                    //_context.BulkMerge(new List<ZoneInLanguage> { entity });

                    return true;
                }
            }
            catch (Exception ex)
            {

            }
            return false;
        }
    }
}
