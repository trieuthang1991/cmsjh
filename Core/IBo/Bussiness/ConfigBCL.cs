﻿
using MI.Dal.IDbContext;
using MI.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MI.Bo.Bussiness
{
    public partial class ConfigBCL : Base<Config>
    {
        public ConfigBCL()
        {

        }

        public List<Config> Get(int pageIndex, int pageSize, string sortBy, string sortDir, out int total)
        {
            using (IDbContext _context = new IDbContext())
            {
                var Config = _context.Config.ToList();

                total = Config.Count();

                return Config.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
            }
        }
    }
}
