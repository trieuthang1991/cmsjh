﻿using Microsoft.EntityFrameworkCore;
using MI.Entity.Models;
using Utils.Settings;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Linq;
using System;
using MI.Entity.Interfaces;

namespace MI.Dal.IDbContext
{
    public class IDbContext : DbContext
    {

        
        public virtual DbSet<ActionInFunctions> ActionInFunctions { get; set; }
        public virtual DbSet<Actions> Actions { get; set; }
        public virtual DbSet<Ads> Ads { get; set; }
        public virtual DbSet<Article> Article { get; set; }
        public virtual DbSet<ArticleInLanguage> ArticleInLanguage { get; set; }
        public virtual DbSet<ArticlesInZone> ArticlesInZone { get; set; }
        public virtual DbSet<AspNetRoleClaims> AspNetRoleClaims { get; set; }
        public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        public virtual DbSet<AspNetUserTokens> AspNetUserTokens { get; set; }
        public virtual DbSet<Config> Config { get; set; }
        public virtual DbSet<ConfigInLanguage> ConfigInLanguage { get; set; }
        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<Department> Department { get; set; }
        public virtual DbSet<DepartmentInLanguage> DepartmentInLanguage { get; set; }
        public virtual DbSet<FileUpload> FileUpload { get; set; }
        public virtual DbSet<Functions> Functions { get; set; }
        public virtual DbSet<Language> Language { get; set; }
        public virtual DbSet<Location> Location { get; set; }
        public virtual DbSet<LocationInLanguage> LocationInLanguage { get; set; }
        public virtual DbSet<Manufacturer> Manufacturer { get; set; }
        public virtual DbSet<ManufacturerInLanguage> ManufacturerInLanguage { get; set; }
        public virtual DbSet<Menu> Menu { get; set; }
        public virtual DbSet<OrderDetail> OrderDetail { get; set; }
        public virtual DbSet<OrderPromotionDetail> OrderPromotionDetail { get; set; }
        public virtual DbSet<Orders> Orders { get; set; }
        public virtual DbSet<Permission> Permission { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<ProductInArticle> ProductInArticle { get; set; }
        public virtual DbSet<ProductInLanguage> ProductInLanguage { get; set; }
        public virtual DbSet<ProductInProduct> ProductInProduct { get; set; }
        public virtual DbSet<ProductInPromotion> ProductInPromotion { get; set; }
        public virtual DbSet<ProductInRegion> ProductInRegion { get; set; }
        public virtual DbSet<ProductInZone> ProductInZone { get; set; }
        public virtual DbSet<ProductPriceInLocation> ProductPriceInLocation { get; set; }
        public virtual DbSet<ProductSpecifications> ProductSpecifications { get; set; }
        public virtual DbSet<ProductSpecificationTemplate> ProductSpecificationTemplate { get; set; }
        public virtual DbSet<ProductSpecificationTemplateInLanguage> ProductSpecificationTemplateInLanguage { get; set; }
        public virtual DbSet<Promotion> Promotion { get; set; }
        public virtual DbSet<PromotionInLanguage> PromotionInLanguage { get; set; }
        public virtual DbSet<Property> Property { get; set; }
        public virtual DbSet<Rating> Rating { get; set; }
        public virtual DbSet<Tag> Tag { get; set; }
        public virtual DbSet<TagInProductLanguage> TagInProductLanguage { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserPermission> UserPermission { get; set; }
        public virtual DbSet<Zone> Zone { get; set; }
        public virtual DbSet<ZoneInLanguage> ZoneInLanguage { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ActionInFunctions>(entity =>
            {
                entity.HasKey(e => new { e.FunctionId, e.ActionId });

                entity.Property(e => e.FunctionId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ActionId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Action)
                    .WithMany(p => p.ActionInFunctions)
                    .HasForeignKey(d => d.ActionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ActionInFunctions_Actions");

                entity.HasOne(d => d.Function)
                    .WithMany(p => p.ActionInFunctions)
                    .HasForeignKey(d => d.FunctionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ActionInFunctions_Functions");
            });

            modelBuilder.Entity<Actions>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Ads>(entity =>
            {
                entity.Property(e => e.Content).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(255);

                entity.Property(e => e.Thumb).HasMaxLength(255);

                entity.Property(e => e.Url).HasMaxLength(255);
            });

            modelBuilder.Entity<Article>(entity =>
            {
                entity.Property(e => e.Avatar).HasMaxLength(500);

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DistributionDate).HasColumnType("datetime");

                entity.Property(e => e.LastModifiedBy)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.LastModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PublishedBy)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Url).HasMaxLength(500);
            });

            modelBuilder.Entity<ArticleInLanguage>(entity =>
            {
                entity.HasKey(e => new { e.LanguageCode, e.ArticleId });

                entity.Property(e => e.LanguageCode).HasMaxLength(10);

                entity.Property(e => e.Author).HasMaxLength(500);

                entity.Property(e => e.Body)
                    .IsRequired()
                    .HasColumnType("ntext");

                entity.Property(e => e.Description).IsRequired();

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.IsAllowComment).HasDefaultValueSql("((1))");

                entity.Property(e => e.MetaDescription).HasMaxLength(255);

                entity.Property(e => e.MetaKeyword).HasMaxLength(255);

                entity.Property(e => e.MetaTitle).HasMaxLength(255);

                entity.Property(e => e.SocialDescription).HasMaxLength(500);

                entity.Property(e => e.SocialImage).HasMaxLength(255);

                entity.Property(e => e.SocialTitle).HasMaxLength(255);

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.Url).HasMaxLength(500);

                entity.Property(e => e.ViewCount).HasDefaultValueSql("((0))");

                entity.HasOne(d => d.Article)
                    .WithMany(p => p.ArticleInLanguage)
                    .HasForeignKey(d => d.ArticleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ArticleInLanguage_Article");

                entity.HasOne(d => d.LanguageCodeNavigation)
                    .WithMany(p => p.ArticleInLanguage)
                    .HasForeignKey(d => d.LanguageCode)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ArticleInLanguage_Language");
            });

            modelBuilder.Entity<ArticlesInZone>(entity =>
            {
                entity.HasKey(e => new { e.ArticleId, e.ZoneId });

                entity.HasOne(d => d.Article)
                    .WithMany(p => p.ArticlesInZone)
                    .HasForeignKey(d => d.ArticleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ArticlesInZone_Article");

                entity.HasOne(d => d.Zone)
                    .WithMany(p => p.ArticlesInZone)
                    .HasForeignKey(d => d.ZoneId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ArticlesInZone_Zone");
            });

            modelBuilder.Entity<AspNetRoleClaims>(entity =>
            {
                entity.HasOne(d => d.Role)
                    .WithMany(p => p.AspNetRoleClaims)
                    .HasForeignKey(d => d.RoleId);
            });

            modelBuilder.Entity<AspNetRoles>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name).HasMaxLength(256);

                entity.Property(e => e.NormalizedName).HasMaxLength(256);
            });

            modelBuilder.Entity<AspNetUserClaims>(entity =>
            {
                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserClaims)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserLogins>(entity =>
            {
                entity.HasKey(e => new { e.LoginProvider, e.ProviderKey });

                entity.Property(e => e.LoginProvider).HasMaxLength(128);

                entity.Property(e => e.ProviderKey).HasMaxLength(128);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserLogins)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUsers>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Address).HasMaxLength(250);

                entity.Property(e => e.Email).HasMaxLength(256);

                entity.Property(e => e.FullName).HasMaxLength(250);

                entity.Property(e => e.NormalizedEmail).HasMaxLength(256);

                entity.Property(e => e.NormalizedUserName).HasMaxLength(256);

                entity.Property(e => e.UserName).HasMaxLength(256);
            });

            modelBuilder.Entity<AspNetUserTokens>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.LoginProvider, e.Name });

                entity.Property(e => e.LoginProvider).HasMaxLength(128);

                entity.Property(e => e.Name).HasMaxLength(128);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserTokens)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<Config>(entity =>
            {
                entity.HasKey(e => e.ConfigGroupKey);

                entity.Property(e => e.ConfigGroupKey)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.ConfigLabel).HasMaxLength(200);

                entity.Property(e => e.ConfigName)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Page).HasMaxLength(20);
            });

            modelBuilder.Entity<ConfigInLanguage>(entity =>
            {
                entity.HasKey(e => new { e.LanguageCode, e.ConfigId });

                entity.Property(e => e.LanguageCode).HasMaxLength(10);
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.Property(e => e.Gender)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.MetaData).HasColumnType("ntext");

                entity.Property(e => e.Name).HasMaxLength(200);

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Department>(entity =>
            {
                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Latitude).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Longitude).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Url)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<DepartmentInLanguage>(entity =>
            {
                entity.HasKey(e => new { e.LanguageCode, e.DepartmentId });

                entity.Property(e => e.LanguageCode).HasMaxLength(10);

                entity.Property(e => e.Address).HasMaxLength(255);

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.Name).HasMaxLength(255);

                entity.Property(e => e.Url).HasMaxLength(255);
            });

            modelBuilder.Entity<FileUpload>(entity =>
            {
                entity.Property(e => e.Description).HasMaxLength(2000);

                entity.Property(e => e.Dimensions)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.FileDownloadPath)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.FileExt)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.FilePath)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.FileSize).HasDefaultValueSql("((0))");

                entity.Property(e => e.LastModifiedBy).HasMaxLength(50);

                entity.Property(e => e.LastModifiedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Name).HasMaxLength(200);

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.Property(e => e.Type).HasDefaultValueSql("((1))");

                entity.Property(e => e.UploadedBy).HasMaxLength(50);

                entity.Property(e => e.UploadedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<Functions>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CssClass).HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ParentId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Url).HasMaxLength(50);
            });

            modelBuilder.Entity<Language>(entity =>
            {
                entity.HasKey(e => e.LanguageCode);

                entity.Property(e => e.LanguageCode)
                    .HasMaxLength(10)
                    .ValueGeneratedNever();

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.SetDefault).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<Location>(entity =>
            {
                entity.Property(e => e.Code).HasMaxLength(10);

                entity.Property(e => e.LanguageCode).HasMaxLength(10);

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.Note).HasMaxLength(255);
            });

            modelBuilder.Entity<LocationInLanguage>(entity =>
            {
                entity.HasKey(e => new { e.LanguageCode, e.LocationId });

                entity.Property(e => e.LanguageCode).HasMaxLength(10);

                entity.Property(e => e.Name).HasMaxLength(255);
            });

            modelBuilder.Entity<Manufacturer>(entity =>
            {
                entity.Property(e => e.Avatar).HasMaxLength(500);

                entity.Property(e => e.Url).HasMaxLength(255);
            });

            modelBuilder.Entity<ManufacturerInLanguage>(entity =>
            {
                entity.HasKey(e => new { e.LanguageCode, e.ManufacturerId });

                entity.Property(e => e.LanguageCode).HasMaxLength(10);

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.Name).HasMaxLength(200);

                entity.Property(e => e.Url).HasMaxLength(255);
            });

            modelBuilder.Entity<Menu>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name).HasMaxLength(255);

                entity.Property(e => e.Path)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<OrderDetail>(entity =>
            {
                entity.Property(e => e.LogPrice).HasColumnType("decimal(18, 0)");
            });

            modelBuilder.Entity<OrderPromotionDetail>(entity =>
            {
                entity.Property(e => e.LogName).HasMaxLength(500);

                entity.Property(e => e.LogType)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LogValue).HasColumnType("decimal(18, 0)");
            });

            modelBuilder.Entity<Orders>(entity =>
            {
                entity.Property(e => e.CreatedBy).HasMaxLength(500);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Permission>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(500);
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.Property(e => e.Avatar)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.AvatarArray)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Code).HasMaxLength(20);

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifyBy)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.PropertyId).HasMaxLength(500);

                entity.Property(e => e.Unit).HasMaxLength(20);

                entity.Property(e => e.Url)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Warranty).HasMaxLength(50);
            });

            modelBuilder.Entity<ProductInArticle>(entity =>
            {
                entity.HasKey(e => new { e.ProductId, e.ArticleId });

                entity.HasOne(d => d.Article)
                    .WithMany(p => p.ProductInArticle)
                    .HasForeignKey(d => d.ArticleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProductInArticle_Article");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductInArticle)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProductInArticle_Product");
            });

            modelBuilder.Entity<ProductInLanguage>(entity =>
            {
                entity.Property(e => e.Id).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Catalog).HasMaxLength(250);

                entity.Property(e => e.Content).HasColumnType("ntext");

                entity.Property(e => e.Description).HasColumnType("ntext");

                entity.Property(e => e.LanguageCode)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.MetaDescription).HasMaxLength(255);

                entity.Property(e => e.MetaKeyword).HasMaxLength(255);

                entity.Property(e => e.MetaTitle).HasMaxLength(255);

                entity.Property(e => e.PromotionInfo).HasMaxLength(500);

                entity.Property(e => e.SocialDescription).HasMaxLength(500);

                entity.Property(e => e.SocialImage).HasMaxLength(255);

                entity.Property(e => e.SocialTitle).HasMaxLength(255);

                entity.Property(e => e.Title).HasMaxLength(225);

                entity.Property(e => e.Url)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ViewCount).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.LanguageCodeNavigation)
                    .WithMany(p => p.ProductInLanguage)
                    .HasForeignKey(d => d.LanguageCode)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProductInLanguage_Language");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductInLanguage)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProductInLanguage_Product");
            });

            modelBuilder.Entity<ProductInProduct>(entity =>
            {
                entity.Property(e => e.Type)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ProductInPromotion>(entity =>
            {
                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductInPromotion)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("FK_ProductInPromotion_Product");

                entity.HasOne(d => d.Promotion)
                    .WithMany(p => p.ProductInPromotion)
                    .HasForeignKey(d => d.PromotionId)
                    .HasConstraintName("FK_ProductInPromotion_Promotion");
            });

            modelBuilder.Entity<ProductInRegion>(entity =>
            {
                entity.Property(e => e.BigThumb)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Name).HasMaxLength(255);

                entity.Property(e => e.Region).HasMaxLength(50);
            });

            modelBuilder.Entity<ProductInZone>(entity =>
            {
                entity.HasKey(e => new { e.ZoneId, e.ProductId, e.IsPrimary });

                entity.Property(e => e.BigThumb)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductInZone)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProductInZone_Product");

                entity.HasOne(d => d.Zone)
                    .WithMany(p => p.ProductInZone)
                    .HasForeignKey(d => d.ZoneId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProductInZone_Zone");
            });

            modelBuilder.Entity<ProductPriceInLocation>(entity =>
            {
                entity.HasKey(e => new { e.LocationId, e.ProductId });

                entity.Property(e => e.Id).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<ProductSpecifications>(entity =>
            {
                entity.Property(e => e.IsShowLayout).HasColumnName("isShowLayout");

                entity.Property(e => e.Value).HasMaxLength(255);
            });

            modelBuilder.Entity<ProductSpecificationTemplate>(entity =>
            {
                entity.Property(e => e.Value).HasMaxLength(255);
            });

            modelBuilder.Entity<ProductSpecificationTemplateInLanguage>(entity =>
            {
                entity.HasKey(e => new { e.LanguageCode, e.ProductSpecificationTemplateId });

                entity.Property(e => e.LanguageCode).HasMaxLength(10);

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.Name).HasMaxLength(255);
            });

            modelBuilder.Entity<Promotion>(entity =>
            {
                entity.Property(e => e.IsDiscountPrice).HasDefaultValueSql("((0))");

                entity.Property(e => e.Name).HasMaxLength(200);

                entity.Property(e => e.Type)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('default')");

                entity.Property(e => e.Value)
                    .HasColumnType("decimal(18, 0)")
                    .HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<PromotionInLanguage>(entity =>
            {
                entity.Property(e => e.Description).HasMaxLength(200);

                entity.Property(e => e.LanguageCode)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Name).HasMaxLength(200);

                entity.HasOne(d => d.Promotion)
                    .WithMany(p => p.PromotionInLanguage)
                    .HasForeignKey(d => d.PromotionId)
                    .HasConstraintName("FK_PromotionInLanguage_Promotion");
            });

            modelBuilder.Entity<Property>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Content).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(500);

                entity.Property(e => e.Thumb).HasMaxLength(500);
            });

            modelBuilder.Entity<Rating>(entity =>
            {
                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Rate).HasColumnType("decimal(18, 1)");
            });

            modelBuilder.Entity<Tag>(entity =>
            {
                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.EditedBy)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LanguageCode).HasMaxLength(10);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(250);

                entity.Property(e => e.Url)
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TagInProductLanguage>(entity =>
            {
                entity.HasKey(e => new { e.TagId, e.ProductInLanguageId });

                entity.Property(e => e.ProductInLanguageId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Tag)
                    .WithMany(p => p.TagInProductLanguage)
                    .HasForeignKey(d => d.TagId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TagInProduct_Tag");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.ActiveCode)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Address).HasMaxLength(255);

                entity.Property(e => e.Avatar)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Email)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.FullName)
                    .HasMaxLength(255)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.IsSystem).HasDefaultValueSql("((0))");

                entity.Property(e => e.LastChangePass).HasColumnType("datetime");

                entity.Property(e => e.LastLogined).HasColumnType("datetime");

                entity.Property(e => e.LoginType).HasDefaultValueSql("((0))");

                entity.Property(e => e.Mobile)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.OtpSecretKey)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SocialId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UserPermission>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.PermissionId, e.ZoneId });
            });

            modelBuilder.Entity<Zone>(entity =>
            {
                entity.Property(e => e.Avatar).HasMaxLength(500);

                entity.Property(e => e.Background).HasMaxLength(200);

                entity.Property(e => e.Banner).HasMaxLength(200);

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Icon).HasMaxLength(255);

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.SortOrder).HasDefaultValueSql("((2))");

                entity.Property(e => e.Url)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ZoneInLanguage>(entity =>
            {
                entity.HasKey(e => new { e.LanguageCode, e.ZoneId });

                entity.Property(e => e.LanguageCode).HasMaxLength(10);

                entity.Property(e => e.BannerLink).HasMaxLength(200);

                entity.Property(e => e.Content).HasColumnType("ntext");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.MetaDescription).HasMaxLength(255);

                entity.Property(e => e.MetaKeyword).HasMaxLength(255);

                entity.Property(e => e.MetaTitle).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Url)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.LanguageCodeNavigation)
                    .WithMany(p => p.ZoneInLanguage)
                    .HasForeignKey(d => d.LanguageCode)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ZoneInLanguage_Language");

                entity.HasOne(d => d.Zone)
                    .WithMany(p => p.ZoneInLanguage)
                    .HasForeignKey(d => d.ZoneId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ZoneInLanguage_Zone");
            });
        }

        public override int SaveChanges()
        {
            var modified = ChangeTracker.Entries()
                .Where(e => e.State == EntityState.Modified || e.State == EntityState.Added);

            foreach (EntityEntry item in modified)
            {
                var changedOrAddedItem = item.Entity as IDateTracking;
                if (changedOrAddedItem != null)
                {
                    if (item.State == EntityState.Added)
                    {
                        changedOrAddedItem.DateCreated = DateTime.Now;
                    }

                    changedOrAddedItem.DateModified = DateTime.Now;
                }
            }

            return base.SaveChanges();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {

                optionsBuilder.UseSqlServer(AppSettings.ConnectionString);
            }
        }
    }
}