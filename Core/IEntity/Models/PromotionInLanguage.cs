﻿using System;
using System.Collections.Generic;

namespace MI.Entity.Models
{
    public partial class PromotionInLanguage
    {
        public int Id { get; set; }
        public int? PromotionId { get; set; }
        public string LanguageCode { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public Promotion Promotion { get; set; }
    }
}
