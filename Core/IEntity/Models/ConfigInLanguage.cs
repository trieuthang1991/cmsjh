﻿using System;
using System.Collections.Generic;

namespace MI.Entity.Models
{
    public partial class ConfigInLanguage
    {
        public string Content { get; set; }
        public string LanguageCode { get; set; }
        public int ConfigId { get; set; }
    }
}
