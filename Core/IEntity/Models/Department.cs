﻿using System;
using System.Collections.Generic;

namespace MI.Entity.Models
{
    public partial class Department
    {
        public int Id { get; set; }
        public decimal? Longitude { get; set; }
        public decimal? Latitude { get; set; }
        public int? SortOrder { get; set; }
        public string Url { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? LocationId { get; set; }
    }
}
