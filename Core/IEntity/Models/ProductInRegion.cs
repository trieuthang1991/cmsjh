﻿using System;
using System.Collections.Generic;

namespace MI.Entity.Models
{
    public partial class ProductInRegion
    {
        public int? ProductId { get; set; }
        public string Region { get; set; }
        public int? SortOrder { get; set; }
        public int? ZoneId { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public bool? IsHot { get; set; }
        public string BigThumb { get; set; }
    }
}
