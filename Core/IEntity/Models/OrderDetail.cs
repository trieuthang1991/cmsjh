﻿using System;
using System.Collections.Generic;

namespace MI.Entity.Models
{
    public partial class OrderDetail
    {
        public int Id { get; set; }
        public int? OrderId { get; set; }
        public int? ProductId { get; set; }
        public int? Quantity { get; set; }
        public decimal? LogPrice { get; set; }
    }
}
