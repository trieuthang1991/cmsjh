﻿using System;
using System.Collections.Generic;

namespace MI.Entity.Models
{
    public partial class Property
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public string Thumb { get; set; }
        public byte GroupId { get; set; }
    }
}
