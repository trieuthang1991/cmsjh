﻿using System;
using System.Collections.Generic;

namespace MI.Entity.Models
{
    public partial class Customer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public string PhoneNumber { get; set; }
        public string Note { get; set; }
        public string MetaData { get; set; }
    }
}
