﻿using System;
using System.Collections.Generic;

namespace MI.Entity.Models
{
    public partial class Promotion
    {
        public Promotion()
        {
            ProductInPromotion = new HashSet<ProductInPromotion>();
            PromotionInLanguage = new HashSet<PromotionInLanguage>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public bool? IsDiscountPrice { get; set; }
        public decimal? Value { get; set; }

        public ICollection<ProductInPromotion> ProductInPromotion { get; set; }
        public ICollection<PromotionInLanguage> PromotionInLanguage { get; set; }
    }
}
