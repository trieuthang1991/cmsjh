﻿using System;
using System.Collections.Generic;

namespace MI.Entity.Models
{
    public partial class Orders
    {
        public int Id { get; set; }
        public int? CustomerId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string Status { get; set; }
    }
}
