﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MI.Entity.Models
{
    public partial class Zone
    {
        public virtual List<ArticlesInZone> ArticlesInZone { get; set; }
        public virtual List<ProductInZone> ProductInZone { get; set; }
        public virtual List<ZoneInLanguage> ZoneInLanguage { get; set; }
        public Zone()
        {
            this.Status = 1;
            this.Type = 0;
            ArticlesInZone = new List<ArticlesInZone>();
            ProductInZone = new List<ProductInZone>();
            ZoneInLanguage = new List<ZoneInLanguage>();
        }

    }
    public partial class ZoneInLanguage
    {
        public ZoneInLanguage()
        {
            this.LanguageCode = "vi-VN     ";
            this.LanguageCodeNavigation = new Language();
            this.MetaDescription = string.Empty;
            this.MetaKeyword = string.Empty;
            this.MetaTitle = string.Empty;
            this.Name = string.Empty;
            this.Url = string.Empty;
            this.Zone = new Zone();
            this.ZoneId = 0;
        }

    }
}
