﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MI.Entity.Models
{
    public partial class Product
    {
        public Product()
        {
            ProductInArticle = new List<ProductInArticle>();
            ProductInLanguage = new List<ProductInLanguage>();
            ProductInZone = new List<ProductInZone>();
            ProductInPromotion = new List<ProductInPromotion>();
        }
        public virtual List<ProductInArticle> ProductInArticle { get; set; }
        public virtual List<ProductInLanguage> ProductInLanguage { get; set; }
        public virtual List<ProductInZone> ProductInZone { get; set; }
        public virtual List<ProductInPromotion> ProductInPromotion { get; set; }
        
    }
}
