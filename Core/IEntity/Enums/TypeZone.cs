﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MI.Entity.Enums
{
    public enum TypeZone : byte
    {
        [EnumDescription("Tất cả")]
        All = 0,
        [EnumDescription("Sản phẩm")]
        Product = 1,
        [EnumDescription("Bài viết")]
        Article = 2,
    }
    public enum StatusZone : byte
    {
        [EnumDescription("Tất cả")]
        All = 0,
        [EnumDescription("Bình thường")]
        Normal = 1,
        [EnumDescription("Ẩn trên web")]
        HiddenWeb = 2,
        [EnumDescription("Trạng thái xóa")]
        Delete = 3,
    }
}
