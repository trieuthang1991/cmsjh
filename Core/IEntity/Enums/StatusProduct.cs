﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MI.Entity.Enums
{
    public enum StatusProduct : byte
    {
        [EnumDescription("Tất cả")]
        All = 0,
        [EnumDescription("Chưa xuất bản")]
        NotPublic = 1,
        [EnumDescription("Đã xuất bản")]
        Public = 2,
        [EnumDescription("Đã xóa")]
        Deleted = 3,
    }
    public enum ProductUnit : byte
    {
        [EnumDescription("mm")]
        All = 0,
        [EnumDescription("m2")]
        NotPublic = 1,
        [EnumDescription("m dài")]
        Public = 2,
    }
}
