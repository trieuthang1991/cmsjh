﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MI.Entity.Enums
{
    public enum TypeArticle : byte
    {
        [EnumDescription("Tất cả")]
        All = 0,
        [EnumDescription("Bài viết sản phẩm")]
        Product = 1,
        [EnumDescription("Bài viết tuyển dụng")]
        Recruitment = 2,
    }
}
