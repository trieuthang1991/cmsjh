﻿R.Product = {
    Init: function () {
        //R.Test();
        R.Product.BindingTotal();
        R.Product.RegisterEvent();
        R.Product.culture = R.Culture();
    },
    RegisterEvent: function () {
        $('.select-zone').off('click').on('click', function () {
            console.log(1);
            var el = $(this);
            var zone_id = $(this).data('id');
            //load ajax
            R.Product.BindingProduct(zone_id, el);
        })
        $('.view-more').off('click').on('click', function () {
            console.log(1);
            var el = $(this);
            var id = $(this).data('id');
            var skip = $(this).data('skip');
            var size = $(this).data('size');
            R.Product.ViewMore(id, el, skip, size);
        })
    },
    BindingTotal: function () {
        $('.set-total').each(function (el) {
            var total = $(this).data('total');
            var id = $(this).data('id');
            console.log(total);
            if (total <= 9)
                $(this).closest('.container').find('.tong_so_sp').parent().hide();
            else {
                $(this).closest('.container').find('.tong_so_sp').text(total - 9);
                $(this).closest('.container').find('.tong_so_sp').parent().show();
                $(this).closest('.container').find('.tong_so_sp').parent().data('size', total - 9);
                $(this).closest('.container').find('.tong_so_sp').parent().data('id', id);
            }
                
        })
    },
    BindingProduct: function (id, el) {
        console.log(id);
        var params = {
            zone_id: id,
            location_id: 3
        }
        $.post(R.Product.culture +'/Product/GetProductByZoneId', params, function (response) {
            console.log(response);
            el.closest('.container').find('._binding_product').html(response);
            R.Product.BindingTotal();
            R.Product.RegisterEvent();
        })
    },
    ViewMore: function (id, el, skip, size) {
        //int zone_parent_id, int locationId, int skip, int size
        var params = {
            zone_parent_id: id,
            locationId : 3,
            skip : skip,
            size : size
        }
        $.post(R.Product.culture + '/Product/ViewMore', params, function (response) {
            el.closest('.container').find('._binding_product').append(response);
            el.hide();
            R.Product.RegisterEvent();
        })
    }
}
$(function () {
    R.Product.Init();
})