﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using JanHome.Web.Models;
using JanHome.Web.Services.Zone.Repository;
using Microsoft.Extensions.Localization;
using Microsoft.AspNetCore.Localization;
using JanHome.Web.Services.Locations.Repository;
using JanHome.Web.Utility;
using Microsoft.AspNetCore.Session;
using Microsoft.AspNetCore.Http;

namespace JanHome.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IZoneRepository _zoneRepository;
        private readonly ILocationsRepository _locationsRepository;
        
        private readonly ISession session;
        const string SessionLocationId = "_LocationId";
        const string SessionLocationName = "_LocationName";
        private string _currentLanguage;
        private string _currentLanguageCode;
        private string CurrentLanguage
        {
            get
            {
                if (!string.IsNullOrEmpty(_currentLanguage))
                    return _currentLanguage;

                if (string.IsNullOrEmpty(_currentLanguage))
                {
                    var feature = HttpContext.Features.Get<IRequestCultureFeature>();
                    _currentLanguage = feature.RequestCulture.Culture.TwoLetterISOLanguageName.ToLower();
                }

                return _currentLanguage;
            }
        }
        private string CurrentLanguageCode
        {
            get
            {
                if (!string.IsNullOrEmpty(_currentLanguageCode))
                    return _currentLanguageCode;

                if (string.IsNullOrEmpty(_currentLanguageCode))
                {
                    var feature = HttpContext.Features.Get<IRequestCultureFeature>();
                    _currentLanguageCode = feature.RequestCulture.Culture.ToString();
                }

                return _currentLanguageCode;
            }


        }
        public HomeController(IZoneRepository zoneRepository, ILocationsRepository locationsRepository, IHttpContextAccessor httpContextAccessor)
        {
            _zoneRepository = zoneRepository;
            _locationsRepository = locationsRepository;
            this.session = httpContextAccessor.HttpContext.Session;
        }
        public ActionResult RedirectToDefaultCulture()
        {
            var culture = CurrentLanguage;
            if (string.IsNullOrEmpty(culture))
                culture = "vi";

            return RedirectToAction("IndexPublic");
        }
        public IActionResult Index()
        {
            
            return View();
        }

        public IActionResult IndexPublic()
        {
            var location_default = _locationsRepository.GetLocationFirst(CurrentLanguageCode);
            if (location_default != null) {
                if (this.session.GetInt32(SessionLocationId) == null || this.session.GetString(SessionLocationName) == null) {
                    this.session.SetInt32(SessionLocationId, location_default.Id);
                    this.session.SetString(SessionLocationName, location_default.Name);
                }
            }            
            
            //var languageCode = CurrentLanguageCode;
            //var zone = _zoneRepository.GetZoneByTreeViewMinifies(1, _currentLanguageCode, 0);
            ////var isHome = true;
            //ViewBag.Zone = zone;
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
