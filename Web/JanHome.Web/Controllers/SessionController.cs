﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JanHome.Web.Services.Locations.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;

namespace JanHome.Web.Controllers
{
    public class SessionController : Controller
    {
        private readonly ILocationsRepository _locationsRepository;

        private readonly ISession session;
        const string SessionLocationId = "_LocationId";
        const string SessionLocationName = "_LocationName";
        private string _currentLanguage;
        private string _currentLanguageCode;
        private string CurrentLanguage
        {
            get
            {
                if (!string.IsNullOrEmpty(_currentLanguage))
                    return _currentLanguage;

                if (string.IsNullOrEmpty(_currentLanguage))
                {
                    var feature = HttpContext.Features.Get<IRequestCultureFeature>();
                    _currentLanguage = feature.RequestCulture.Culture.TwoLetterISOLanguageName.ToLower();
                }

                return _currentLanguage;
            }
        }
        private string CurrentLanguageCode
        {
            get
            {
                if (!string.IsNullOrEmpty(_currentLanguageCode))
                    return _currentLanguageCode;

                if (string.IsNullOrEmpty(_currentLanguageCode))
                {
                    var feature = HttpContext.Features.Get<IRequestCultureFeature>();
                    _currentLanguageCode = feature.RequestCulture.Culture.ToString();
                }

                return _currentLanguageCode;
            }


        }
        public SessionController(ILocationsRepository locationsRepository, IHttpContextAccessor httpContextAccessor)
        {
            _locationsRepository = locationsRepository;
            this.session = httpContextAccessor.HttpContext.Session;
        }
        [HttpPost]
        public IActionResult ChangeCurrentLocation(int location_id, string location_name, string currentUrl)
        {
            this.session.SetInt32(SessionLocationId, location_id);
            this.session.SetString(SessionLocationName, location_name);
            return Ok();
        }
    }
}