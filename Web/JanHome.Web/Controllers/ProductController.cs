﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JanHome.Web.Services.Product.Repository;
using JanHome.Web.Services.Zone.Repository;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace JanHome.Web.Controllers
{
    public class ProductController : Controller
    {
        private readonly IZoneRepository _zoneRepository;
        private readonly IProductRepository _productRepository;
        private readonly IStringLocalizer<HomeController> _localizer;
        private string _currentLanguage;
        private string _currentLanguageCode;
        private string CurrentLanguage
        {
            get
            {
                if (!string.IsNullOrEmpty(_currentLanguage))
                    return _currentLanguage;

                if (string.IsNullOrEmpty(_currentLanguage))
                {
                    var feature = HttpContext.Features.Get<IRequestCultureFeature>();
                    _currentLanguage = feature.RequestCulture.Culture.TwoLetterISOLanguageName.ToLower();
                }

                return _currentLanguage;
            }
        }
        private string CurrentLanguageCode
        {
            get
            {
                if (!string.IsNullOrEmpty(_currentLanguageCode))
                    return _currentLanguageCode;

                if (string.IsNullOrEmpty(_currentLanguageCode))
                {
                    IRequestCultureFeature feature = HttpContext.Features.Get<IRequestCultureFeature>();
                    _currentLanguageCode = feature.RequestCulture.Culture.ToString();
                }

                return _currentLanguageCode;
            }


        }
        public ProductController(IZoneRepository zoneRepository, IProductRepository productRepository, IStringLocalizer<HomeController> localizer)
        {
            _localizer = localizer;
            _zoneRepository = zoneRepository;
            _productRepository = productRepository;
        }
        public IActionResult ProductList(int zoneId)
        {
            var zone_selected_with_treeview = _zoneRepository.GetZoneByTreeViewMinifies(1, CurrentLanguageCode, zoneId);
            var zone_details = _zoneRepository.GetZoneDetail(zoneId, CurrentLanguageCode);
            ViewBag.ZoneTreeView = zone_selected_with_treeview;
            ViewBag.ZoneDetail = zone_details;

            return View();
        }
        //[HttpPost]
        public IActionResult ProductDetail(int product_id) {
            var product_infomatin_detail = _productRepository.GetProductInfomationDetail(product_id, CurrentLanguageCode);
            var product_spectification_detail = _productRepository.GetProductSpectificationDetail(product_id, CurrentLanguageCode);
            var same_zone_total = 0;
            var product_same_zone = _productRepository.GetProductInZoneByZoneIdMinify(product_infomatin_detail.ZoneId,3, CurrentLanguageCode, 1, 6, out same_zone_total);
            ViewBag.Infomation = product_infomatin_detail;
            ViewBag.Zone = product_infomatin_detail.ZoneId;
            ViewBag.Spectification = product_spectification_detail;
            ViewBag.SameZone = product_same_zone;
            ViewBag.SameTotal = same_zone_total;
            return View();
        }

        [HttpPost]
        public IActionResult GetProductByZoneId(int zone_Id, int location_id)
        {
            return ViewComponent("ProductList", new { zone_parent_id = zone_Id, locationId = location_id });
        }
        [HttpPost]
        public IActionResult ViewMore(int zone_parent_id, int locationId, int skip, int size)
        {
            return ViewComponent("ViewMore", new { zone_parent_id = zone_parent_id, locationId = locationId, skip = skip, size = size });
        }
    }
}