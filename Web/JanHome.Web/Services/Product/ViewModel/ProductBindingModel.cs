﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JanHome.Web.Services.Product.ViewModel
{
    public class ProductMinify
    {
        public int ZoneId { get; set; }
        public int ProductId { get; set; }
        public string Title { get; set; }
        public string Avatar { get; set; }
        public string Url { get; set; }
        public string PropertyId { get; set; }
        public decimal Price { get; set; }
        public decimal SalePrice { get; set; }
        public int IsHot { get; set; }
        public string BigThumb { get; set; }
        public decimal Rate { get; set; }
        public string SpecName { get; set; }
        public string SpecValue { get; set; }
        public int CountRate { get; set; }
    }

    public class ProductDetail {
        public int Id { get; set; }
        public string AvatarArray { get; set; }
        public string Warranty { get; set; }
        public int ManufacturerId { get; set; }
        public string Code { get; set; }
        public string Unit { get; set; }
        public int Quantity { get; set; }
        public string PropertyId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public string Url { get; set; }
        public string PromotionInfo { get; set; }
        public string Catalog { get; set; }
        public string MetaTitle { get; set; }
        public string MetaKeyword { get; set; }
        public string MetaDescription { get; set; }
        public string SocialTitle { get; set; }
        public string SocialDescription { get; set; }
        public string SocialImage { get; set; }
        public string LanguageCode { get; set; }
        public double RateAVG { get; set; }
        public int TotalRate { get; set; }
        public int ZoneId { get; set; }
    }

    public class ProductSpectificationDetail
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }

    }
}
