﻿using Dapper;
using JanHome.Web.ExecuteCommand;
using JanHome.Web.Services.Product.ViewModel;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JanHome.Web.Services.Product.Repository
{

    public interface IProductRepository 
    {
        List<ProductMinify> GetProductMinifiesTreeViewByZoneParentId(int parentId, string lang_code, int locationId, int pageNumber, int pageSize, out int total);
        List<ProductMinify> GetProductMinifiesTreeViewByZoneParentIdSkipping(int parentId, string lang_code, int locationId, int skip, int size);
        ProductDetail GetProductInfomationDetail(int id, string lang_code);

        List<ProductSpectificationDetail> GetProductSpectificationDetail(int id, string lang_code);
        List<ProductMinify> GetProductInZoneByZoneIdMinify(int zone_id, int locationId, string lang_code, int pageNumber, int pageSize, out int total);
    }
    public class ProductRepository : IProductRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string _connStr;
        private readonly IExecuters _executers;

        public ProductRepository(IConfiguration configuration, IExecuters executers)
        {
            _configuration = configuration;
            _connStr = _configuration.GetConnectionString("DefaultConnection");
            _executers = executers;
        }
        public List<ProductMinify> GetProductMinifiesTreeViewByZoneParentId(int parentId, string lang_code, int locationId, int pageNumber, int pageSize, out int total)
        {
            var p = new DynamicParameters();
            var commandText = "usp_Web_GetProductTreeviewByZoneParentShowLayout";
            p.Add("@parentId", parentId);
            p.Add("@lang_code", lang_code);
            p.Add("@locationId", locationId);
            p.Add("@pageNumber", pageNumber);
            p.Add("@pageSize", pageSize);
            p.Add("@total", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
            var result = _executers.ExecuteCommand(_connStr, conn => conn.Query<ProductMinify>(commandText, p, commandType: System.Data.CommandType.StoredProcedure)).ToList();
            total = p.Get<int>("@total");
            return result;
        }
        public List<ProductMinify> GetProductMinifiesTreeViewByZoneParentIdSkipping(int parentId, string lang_code, int locationId, int skip, int size) {
            var p = new DynamicParameters();
            var commandText = "usp_Web_GetProductTreeviewByZoneParentShowLayout_Skipping";
            p.Add("@parentId", parentId);
            p.Add("@lang_code", lang_code);
            p.Add("@locationId", locationId);
            p.Add("@skip", skip);
            p.Add("@size", size);
            var result = _executers.ExecuteCommand(_connStr, conn => conn.Query<ProductMinify>(commandText, p, commandType: System.Data.CommandType.StoredProcedure)).ToList();
            return result;
        }

        public ProductDetail GetProductInfomationDetail(int id, string lang_code) {
            var p = new DynamicParameters();
            var commandText = "usp_Web_GetProductInfomationDetail";
            p.Add("@id", id);
            p.Add("@lang_code", lang_code);
            var result = _executers.ExecuteCommand(_connStr, conn => conn.QueryFirstOrDefault<ProductDetail>(commandText, p, commandType: System.Data.CommandType.StoredProcedure));
            return result;
        }

        public List<ProductSpectificationDetail> GetProductSpectificationDetail(int id, string lang_code)
        {
            var p = new DynamicParameters();
            var commandText = "usp_Web_GetProductSpecificationDetail";
            p.Add("@id", id);
            p.Add("@lang_code", lang_code);
            var result = _executers.ExecuteCommand(_connStr, conn => conn.Query<ProductSpectificationDetail>(commandText, p, commandType: System.Data.CommandType.StoredProcedure)).ToList();
            return result;
        }

        public List<ProductMinify> GetProductInZoneByZoneIdMinify(int zone_id, int locationId, string lang_code, int pageNumber, int pageSize, out int total) 
        {

            var p = new DynamicParameters();
            var commandText = "usp_Web_GetProductInZoneByZoneId_Minify";
            p.Add("@zone_id", zone_id);
            p.Add("@lang_code", lang_code);
            p.Add("@locationId", locationId);
            p.Add("@pageNumber", pageNumber);
            p.Add("@pageSize", pageSize);
            p.Add("@total", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
            var result = _executers.ExecuteCommand(_connStr, conn => conn.Query<ProductMinify>(commandText, p, commandType: System.Data.CommandType.StoredProcedure)).ToList();
            total = p.Get<int>("@total");
            return result;

        }
    }
}
