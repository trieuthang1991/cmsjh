﻿using Dapper;
using JanHome.Web.ExecuteCommand;

using JanHome.Web.Services.Zone.ViewModal;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Dapper.SqlMapper;
using MI.Entity.Models;

namespace JanHome.Web.Services.Zone.Repository
{
    public interface IZoneRepository {
        List<ZoneByTreeViewMinify> GetZoneByTreeViewMinifies(int type, string lang_code, int parentId);
        ZoneDetail GetZoneDetail(int zoneId, string lang_code);
    }
    public class ZoneRepository : IZoneRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string _connStr;
        private readonly IExecuters _executers;

        public ZoneRepository(IConfiguration configuration, IExecuters executers)
        {
            _configuration = configuration;
            _connStr = _configuration.GetConnectionString("DefaultConnection");
            _executers = executers;
        }

        public List<ZoneByTreeViewMinify> GetZoneByTreeViewMinifies(int type, string lang_code, int parentId)
        {
            var p = new DynamicParameters();
            var commandText = "usp_Web_GetZoneByTreeView_Minify_v1";
            p.Add("@type", type);
            p.Add("@lang_code", lang_code);
            p.Add("@parentId", parentId);
            var result = _executers.ExecuteCommand(_connStr, conn => conn.Query<ZoneByTreeViewMinify>(commandText,p,commandType: System.Data.CommandType.StoredProcedure)).ToList();
            return result;
        }

        public ZoneDetail GetZoneDetail(int zoneId, string lang_code)
        {
            var result = new ZoneDetail();
            var p = new DynamicParameters();
            var commandText = "usp_Web_GetZoneDetail";
            p.Add("@id", zoneId);
            p.Add("@lang_code", lang_code);
            result = _executers.ExecuteCommand(_connStr, conn => conn.QueryFirstOrDefault<ZoneDetail>(commandText, p, commandType: System.Data.CommandType.StoredProcedure));
            return result;
        }
    }
}
